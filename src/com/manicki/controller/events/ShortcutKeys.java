package com.manicki.controller.events;

import com.manicki.controller.events.windows.mainwindow.CloseApplication;
import com.manicki.controller.events.windows.mainwindow.menubar.menuitems.FileMenuEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.Windows;
import com.manicki.view.windows.CreateMessageWindow;
import com.manicki.view.windows.ShowMessageWindow;
import com.manicki.view.windows.mainwindow.components.menubar.menuitems.FileMenuItems;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.WindowEvent;

public class ShortcutKeys {
    private static ShortcutKeys instance = null;
    private MessageBox messageBox;
    private String shortcutKeyCombination,  description;

    private ShortcutKeys() {
        this.messageBox = MessageBox.getInstance();
    }

    public static ShortcutKeys getInstance() {
        if (instance == null) {
            instance = new ShortcutKeys();
        }
        return instance;
    }

    public void closeWindowShortcut(Windows window, Scene scene) {
        shortcutKeyCombination = KeyCode.ESCAPE.getName();
        description = messageBox.getLanguageFileProperty("closeWindow");

        scene.addEventFilter(KeyEvent.KEY_PRESSED, keysPressed -> {
            if (keysPressed.getCode() == KeyCode.ESCAPE) {
                if (window instanceof CreateMessageWindow){
                    ((CreateMessageWindow) window).confirmToCloseWindow(
                            new WindowEvent(
                                    ((CreateMessageWindow) window).getStage(),
                                    WindowEvent.WINDOW_CLOSE_REQUEST
                            )
                    );
                } else {
                    window.close();
                }
            }
        });
    }

    public void changeBoxShortcut(Scene scene) {
        KeyCombination changeBoxCombination = new KeyCodeCombination(KeyCode.TAB, KeyCombination.CONTROL_DOWN);
        shortcutKeyCombination = changeBoxCombination.getDisplayText();
        description = messageBox.getLanguageFileProperty("changeBoxType");

        scene.addEventFilter(KeyEvent.KEY_PRESSED, keyPressed -> {
            if (changeBoxCombination.match(keyPressed)) {
                if (!messageBox.getMainWindowView().getChoiceMessageBoxPane().getChoiceBox().getValue()
                        .equals(messageBox.getLanguageFileProperty("inbox"))) {
                    messageBox.getMainWindowView().getChoiceMessageBoxPane().getChoiceBox()
                            .setValue(messageBox.getLanguageFileProperty("inbox"));
                } else {
                    messageBox.getMainWindowView().getChoiceMessageBoxPane().getChoiceBox()
                            .setValue(messageBox.getLanguageFileProperty("outbox"));
                }
            }
        });
    }

    public void createNewMessageShortcut(Scene scene) {
        KeyCombination keyCombNewMessage = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN);
        shortcutKeyCombination = keyCombNewMessage.getDisplayText();
        shortcutKeyCombination = shortcutKeyCombination.substring(0, shortcutKeyCombination.length() - 1);
        shortcutKeyCombination += KeyCode.ENTER.getName();
        description = messageBox.getLanguageFileProperty("openNewMessageWindow");

        scene.addEventFilter(KeyEvent.KEY_PRESSED, keysPressed -> {
            if (!messageBox.getUsername().isEmpty()) {
                if (keyCombNewMessage.match(keysPressed)) {
                    new CreateMessageWindow().create(null, "none", null);
                }
            }
        });
    }

    public void deleteMessageShortcut(Scene scene) {
        shortcutKeyCombination = KeyCode.DELETE.getName();
        description = messageBox.getLanguageFileProperty("deleteMessageMainWindow");

        scene.addEventFilter(KeyEvent.KEY_PRESSED, keysPressed -> {
            if (!messageBox.getUsername().isEmpty()) {
                if (keysPressed.getCode() == KeyCode.DELETE) {
                    new FileMenuEvents().event("DeleteMessage");
                }
            }
        });
    }

    public void openFindMessageWindow(Scene scene) {
        KeyCombination keyOpenFindMessageWindowCombination = new KeyCodeCombination(KeyCode.F, KeyCombination.CONTROL_DOWN);
        shortcutKeyCombination = keyOpenFindMessageWindowCombination.getDisplayText();
        description = messageBox.getLanguageFileProperty("openFindMessageWindow");

        scene.addEventFilter(KeyEvent.KEY_PRESSED, keyPressed -> {
            if (!messageBox.getUsername().isEmpty()) {
                if (keyOpenFindMessageWindowCombination.match(keyPressed)) {
                    new FileMenuEvents().event("Find");
                }
            }
        });
    }

    public void openMessageShortcut(Scene scene) {
        KeyCombination keyCombNewMessage = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN);
        shortcutKeyCombination = KeyCode.ENTER.getName();
        description = messageBox.getLanguageFileProperty("openMessage");

        scene.addEventFilter(KeyEvent.KEY_PRESSED, keysPressed -> {
            if (!messageBox.getUsername().isEmpty()) {
                if ((!keyCombNewMessage.match(keysPressed))
                        && (keysPressed.getCode() == KeyCode.ENTER)) {
                    new ShowMessageWindow().create();
                }
            }
        });
    }

    public void pushAcceptButtonShortcut(Scene scene, Button acceptButton) {
        shortcutKeyCombination = KeyCode.ENTER.getName();
        description = messageBox.getLanguageFileProperty("accept") + " " + messageBox.getLanguageFileProperty("acceptShortcutDetail");

        scene.addEventHandler(KeyEvent.KEY_PRESSED, keysPressed -> {
            if (keysPressed.getCode() == KeyCode.ENTER) {
                acceptButton.fire();
            }
        });
    }

    public void pushAcceptOrCancelButtonShortcut(Scene scene, Button acceptButton, Button cancelButton) {
        pushAcceptButtonShortcut(scene, acceptButton);
        pushCancelButtonShortcut(scene, cancelButton);
    }

    public void pushCancelButtonShortcut(Scene scene, Button cancelButton) {
        shortcutKeyCombination = KeyCode.ESCAPE.getName();
        description = messageBox.getLanguageFileProperty("cancel");

        scene.addEventHandler(KeyEvent.KEY_PRESSED, keysPressed -> {
            if (keysPressed.getCode() == KeyCode.ESCAPE) {
                cancelButton.fire();
            }
        });
    }

    public void quitApplicationShortcut(Scene scene) {
        KeyCombination quitApplicationKeyCombination = new KeyCodeCombination(KeyCode.Q, KeyCombination.CONTROL_DOWN);
        shortcutKeyCombination = quitApplicationKeyCombination.getDisplayText();
        description = messageBox.getLanguageFileProperty("closeApplication");

        scene.addEventFilter(KeyEvent.KEY_PRESSED, keysPressed -> {
            if (quitApplicationKeyCombination.match(keysPressed)) {
                new CloseApplication().showConfirmation(new WindowEvent(messageBox.getStage(), WindowEvent.WINDOW_CLOSE_REQUEST));
            }
        });
    }

    public void sendMessageShortcut(Scene scene, Button sendButton) {
        KeyCombination sendMessageCombination = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.CONTROL_DOWN);
        shortcutKeyCombination = sendMessageCombination.getDisplayText();
        description = messageBox.getLanguageFileProperty("sendMessage");

        scene.addEventFilter(KeyEvent.KEY_PRESSED, keyPressed -> {
            if (sendMessageCombination.match(keyPressed)) {
                sendButton.fire();
            }
        });
    }

    public void signInOrOutShortcut(Scene scene) {
        KeyCombination signInOutKeyCombination = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
        shortcutKeyCombination = signInOutKeyCombination.getDisplayText();
        description = messageBox.getLanguageFileProperty("signInOut");

        scene.addEventFilter(KeyEvent.KEY_PRESSED, keysPressed -> {
            if (signInOutKeyCombination.match(keysPressed)) {
                messageBox.getMainWindowView().getMenuBar().getMenus().get(0).getItems()
                        .get(FileMenuItems.LOGIN.ordinal()).fire();
            }
        });
    }

    public String getShortcutKeyCombination() {
        return shortcutKeyCombination;
    }

    public String getDescription() {
        return description;
    }
}
