package com.manicki.controller.events;

import com.manicki.controller.Logger;
import com.manicki.controller.errorsandexceptions.Alerts;
import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.controller.errorsandexceptions.DatabaseConnectionException;
import com.manicki.controller.events.windows.mainwindow.LogOutEvent;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.Messages;
import com.manicki.service.database.SqlQuerys;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

public class MessagesEvents {
    private MessageBox messageBox;
    private int newMessagesCounter = 0;
    private int connectionTimeout = 3;

    public MessagesEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public void runThreadToCheckNewMessages() {
        messageBox.setCheckNewMessagesThread(new Timer());
        messageBox.getCheckNewMessagesThread().schedule(new TimerTask() {
            @Override
            public void run() {
                Platform.runLater(() -> {
                    try {
                        int numberOfNewMessages = getNewMessagesCount();
                        if (numberOfNewMessages > newMessagesCounter) {
                            newMessagesCounter = numberOfNewMessages;
                            messageBox.getMainWindowView().getMainWindowEvents()
                                    .refreshTable(new SqlQuerys().getInboxMessages());
                            //TODO Create class for checking state of main window and prepare action when new message is found
                            messageBox.getStage().setAlwaysOnTop(true);
                            messageBox.getStage().setAlwaysOnTop(false);
                            Alert newMessageAlert = new CreateAlert().newMessage();
                            newMessageAlert.show();
                        } else {
                            newMessagesCounter = numberOfNewMessages;
                        }
                    } catch (SQLException | DatabaseConnectionException e) {
                        new LogOutEvent().closeAllOpenedWindows();
                        new LogOutEvent().logout();
                        Alert databaseConnectionBroken = new CreateAlert().databaseConnectionBroken();
                        databaseConnectionBroken.show();
                        Logger.getInstance().logError("Connection broken: \n" + e.getMessage());
                    }
                });
            }
        }, 1000, 5000);
    }

    public void stopThreadToCheckNewMessages() {
        Platform.runLater(() -> {
            if (messageBox.getCheckNewMessagesThread() != null) {
                messageBox.getCheckNewMessagesThread().cancel();
                newMessagesCounter = 0;
            }
        });
    }

    public void deleteMessage() {
        if (messageBox.getMainWindowView().getChoiceMessageBoxPane().getChoiceBoxSelectedItem()
                .equals(messageBox.getLanguageFileProperty("inbox"))) {
            Alert confirmToDeleteMessage = new CreateAlert().confirmToDeleteMessage();
            try {
                Optional<ButtonType> yesNoButton = confirmToDeleteMessage.showAndWait();
                if (yesNoButton.get() == confirmToDeleteMessage.getButtonTypes().get(Alerts.ButtonsType.YES_BUTTON.ordinal())) {
                    new SqlQuerys().setMessageAsDeleted(new Messages().getIdOfSelectedMessage());
                    saveCurrentAndSelectTableRow();
                    messageBox.getMainWindowView().getMainWindowEvents()
                            .refreshTable(new SqlQuerys().getInboxMessages());
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            Alert errorDeletingMessage = new CreateAlert().errorDeletingMessage();
            errorDeletingMessage.show();
        }
    }

    private int getNewMessagesCount() throws SQLException, DatabaseConnectionException {
        int newMessagesCounter = 0;
        if (messageBox.getDatabase().getConnection().isValid(connectionTimeout)) {
            ResultSet resultSet = new SqlQuerys().getNewMessages();
            while (resultSet.next()) {
                newMessagesCounter++;
            }
        } else {
            throw new DatabaseConnectionException();
        }

        return newMessagesCounter;
    }

    public void restoreMessage() {
        try {
            new SqlQuerys().setMessageAsNotDeleted(new Messages().getIdOfSelectedMessage());
            saveCurrentAndSelectTableRow();
            messageBox.getMainWindowView().getMainWindowEvents().refreshTable(new SqlQuerys().getDeletedMessages());
        } catch (SQLException e) {
            Logger.getInstance().logError(e.getMessage());
        }
    }

    private void saveCurrentAndSelectTableRow() {
        int selectedRow = messageBox.getMainWindowView().getMessagesTable().getSelectedRowIndex();
        if (selectedRow > 1) {
            selectedRow--;
        }
        messageBox.getMainWindowView().getMessagesTable().setSelectedRow(selectedRow);
    }
}
