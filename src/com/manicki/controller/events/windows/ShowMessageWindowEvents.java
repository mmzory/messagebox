package com.manicki.controller.events.windows;

import com.manicki.controller.events.ShortcutKeys;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.database.SqlQuerys;
import com.manicki.view.Windows;
import com.manicki.view.windows.CreateMessageWindow;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.sql.SQLException;

public class ShowMessageWindowEvents {
    private MessageBox messageBox;

    public ShowMessageWindowEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public Scene event(Stage stage, Button button, String[] messageContent) {
        Scene scene = new CreateMessageWindow().create(stage, button.getId(), messageContent);
        return scene;
    }

    public void setMessageAsReaded(long id) {
        try {
            messageBox.getMainWindowView().getMessagesTable()
                    .setSelectedRow(messageBox.getMainWindowView().getMessagesTable().getSelectedRowIndex());
            if (messageBox.getMainWindowView().getChoiceMessageBoxPane().getChoiceBoxSelectedItem()
                    .equals(messageBox.getLanguageFileProperty("inbox"))) {
                new SqlQuerys().setMessageAsReaded(id);
                messageBox.getMainWindowView().getMainWindowEvents().refreshTable(new SqlQuerys().getInboxMessages());
            } else  if (messageBox.getMainWindowView().getChoiceMessageBoxPane().getChoiceBoxSelectedItem()
                    .equals(messageBox.getLanguageFileProperty("outbox"))) {
                messageBox.getMainWindowView().getMainWindowEvents().refreshTable(new SqlQuerys().getOutboxMessages());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setShortcutKeys(Windows window, Scene scene) {
        ShortcutKeys shortcutKeys = ShortcutKeys.getInstance();
        shortcutKeys.closeWindowShortcut(window, scene);
    }
}