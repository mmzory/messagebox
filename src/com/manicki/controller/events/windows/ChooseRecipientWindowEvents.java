package com.manicki.controller.events.windows;

import com.manicki.controller.errorsandexceptions.DatabaseConnectionException;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.Users;
import com.manicki.view.windows.ChooseRecipientWindow;
import com.manicki.view.windows.CreateMessageWindow;
import javafx.scene.control.CheckBox;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public class ChooseRecipientWindowEvents {
    private MessageBox messageBox;

    public ChooseRecipientWindowEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public void putRecipientsToCreateMessageWindowField(ChooseRecipientWindow chooseRecipientWindow,
                                                        CreateMessageWindow createMessageWindow,
                                                        List<CheckBox> recipients) {
        String sRecipients = "";
        for (CheckBox box : recipients) {
            if (box.isSelected()) {
                sRecipients += box.getText() + "; ";
            }
        }
        createMessageWindow.setTfRecipientText(sRecipients);
        chooseRecipientWindow.close();
        createMessageWindow.toFront();
    }

    public Set<String> getDatabaseUsers() {
        try {
            Set<String> databaseUsers = new Users().getDatabaseUsers();
            return databaseUsers;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        throw new DatabaseConnectionException();
    }
}
