package com.manicki.controller.events.windows;

import com.manicki.controller.events.ShortcutKeys;
import javafx.scene.Scene;
import javafx.scene.control.Button;

public class ShortcutKeysWindowsEvents {
    public void setShortcutKeys(Scene scene, Button closeButton) {
        ShortcutKeys shortcutKeys = ShortcutKeys.getInstance();
        shortcutKeys.pushCancelButtonShortcut(scene, closeButton);
    }
}
