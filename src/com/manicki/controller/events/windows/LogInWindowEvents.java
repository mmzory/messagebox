package com.manicki.controller.events.windows;

import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.controller.errorsandexceptions.DatabaseConnectionException;
import com.manicki.controller.errorsandexceptions.SQLQueryException;
import com.manicki.controller.events.ShortcutKeys;
import com.manicki.controller.events.windows.mainwindow.AfterLogInEvent;
import com.manicki.service.database.Database;
import com.manicki.view.windows.LogInWindow;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

public class LogInWindowEvents {

    public boolean connectToDatabase(String username, String password) {
        try {
            Database database = new Database();
            database.connectToDatabase(username, password);
            database.createDatabase();
            database.createTables();
            AfterLogInEvent afterLogInEvent = new AfterLogInEvent();
            afterLogInEvent.saveUsernameInConfigFile(username);
            afterLogInEvent.changeMainWindowLook();
            afterLogInEvent.startThreadToCheckNewMessages();
        } catch (DatabaseConnectionException e) {
            Alert alert = new CreateAlert().connectionErrorOrIncorrectData();
            alert.show();
            alert.setOnCloseRequest(event -> requestFocusOnLoginWindowPasswordField());
            return true;
        } catch (SQLQueryException e) {
            Alert alert = new CreateAlert().creatingDatabaseTableError();
            alert.show();
            return true;
        }

        return false;
    }

    public void setShortcutKeys(Scene scene, Button acceptButton, Button cancelButton) {
        ShortcutKeys shortcutKeys = ShortcutKeys.getInstance();
        shortcutKeys.pushAcceptOrCancelButtonShortcut(scene, acceptButton, cancelButton);
    }

    private void requestFocusOnLoginWindowPasswordField() {
        LogInWindow.createOrGetInstance().getPasswordField().requestFocus();
    }
}
