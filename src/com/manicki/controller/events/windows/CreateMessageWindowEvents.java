package com.manicki.controller.events.windows;

import com.manicki.controller.errorsandexceptions.Alerts;
import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.controller.events.ShortcutKeys;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.database.SqlQuerys;
import com.manicki.view.Windows;
import com.manicki.view.windows.ChooseRecipientWindow;
import com.manicki.view.windows.CreateMessageWindow;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.WindowEvent;

import java.sql.SQLException;
import java.util.Optional;

public class CreateMessageWindowEvents {
    private MessageBox messageBox;

    public CreateMessageWindowEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public void sendMessage(CreateMessageWindow createMessageWindow) throws SQLException {
        for (String recipient : createMessageWindow.getTfRecipients().replaceAll(" ", "").split(";")) {
            new SqlQuerys().sendMessage(
                    recipient,
                    createMessageWindow.getTopic(),
                    createMessageWindow.getMessage());
        }
        createMessageWindow.close();
    }

    public String checkIfFieldsAreNotEmpty(TextField recipients, TextField topic, TextArea message) {
        String errorMessage = null;
        boolean error = false;
        if (recipients.getText().length() == 0) {
            errorMessage = messageBox.getLanguageFileProperty("recipients");
            recipients.requestFocus();
            error = true;
        }
        if (topic.getText().length() == 0) {
            if (errorMessage == null) {
                errorMessage = messageBox.getLanguageFileProperty("topic2");
                topic.requestFocus();
            } else {
                errorMessage += ", " + messageBox.getLanguageFileProperty("topic2");
            }
            error = true;
        }

        if (message.getText().length() == 0) {
            if (errorMessage == null) {
                errorMessage = messageBox.getLanguageFileProperty("content");
                message.requestFocus();
            } else {
                errorMessage += " " + messageBox.getLanguageFileProperty("and") + " "
                        + messageBox.getLanguageFileProperty("content");
            }
            error = true;
        }

        if (error) {
            errorMessage = messageBox.getLanguageFileProperty("missed") + " " + errorMessage + " "
                    + messageBox.getLanguageFileProperty("fillDataAndTryAgain");
        }

        return errorMessage;
    }

    public void createOrShowChooseRecipientWindow(CreateMessageWindow createMessageWindow, ChooseRecipientWindow chooseRecipientWindow) {
        if (!createMessageWindow.isChooseRecipientWindowExists()) {
            chooseRecipientWindow = new ChooseRecipientWindow(createMessageWindow);
            chooseRecipientWindow.create();
            createMessageWindow.setChooseRecipientWindow(chooseRecipientWindow);
            createMessageWindow.setChooseRecipientWindowExists(true);
        } else {
            chooseRecipientWindow.toFront();
        }
    }

    public void confirmToCloseWindow(CreateMessageWindow createMessageWindow, WindowEvent closeEvent) {
        Alert confitmToCloseWindow = new CreateAlert().messageFieldIsNotEmpty(createMessageWindow.getStage());
        Optional<ButtonType> yesNoButton = confitmToCloseWindow.showAndWait();
        if (yesNoButton.get() == confitmToCloseWindow.getButtonTypes().get(Alerts.ButtonsType.YES_BUTTON.ordinal())) {
            createMessageWindow.close();
        } else {
            closeEvent.consume();
            createMessageWindow.toFront();
        }
    }

    public void setShortcutKeys(Windows window, Scene scene, Button sendButton) {
        ShortcutKeys shortcutKeys = ShortcutKeys.getInstance();
        shortcutKeys.closeWindowShortcut(window, scene);
        shortcutKeys.sendMessageShortcut(scene, sendButton);
    }
}
