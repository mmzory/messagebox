package com.manicki.controller.events.windows.mainwindow;

import com.manicki.messagebox.MessageBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class HideApplicationPopupMenuEvents {
    private MessageBox messageBox;

    public HideApplicationPopupMenuEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public void showMainWindow() {
        Stage stage = messageBox.getStage();
        stage.setWidth(messageBox.getMainWindowView().getWindowSize().getWidth());
        stage.setHeight(messageBox.getMainWindowView().getWindowSize().getHeight());
        stage.setX(messageBox.getMainWindowView().getWindowLocation().getX());
        stage.setY(messageBox.getMainWindowView().getWindowLocation().getY());
        stage.show();
        messageBox.getMainWindowView().getMainPane().setCenter(messageBox.getMainWindowView().getMessagesTable());
        messageBox.getSystemTray().remove(messageBox.getSystemTray().getTrayIcons()[0]);
        messageBox.getMainWindowView().setHidden(false);
    }

    public void closeMainWindow() {
        new CloseApplication()
                .showConfirmation(new WindowEvent(messageBox.getStage(), WindowEvent.WINDOW_CLOSE_REQUEST));
    }
}
