package com.manicki.controller.events.windows.mainwindow;

import com.manicki.controller.errorsandexceptions.Alerts;
import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.windows.LogInWindow;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.WindowEvent;

import java.util.Optional;

public class CloseApplication {
    private MessageBox messageBox;

    public CloseApplication() {
        this.messageBox = MessageBox.getInstance();
    }

    public void showConfirmation(WindowEvent event) {
        Alert confirmCloseApplication;
        if (messageBox.getMainWindowView().isAnyWindowOpen()) {
            confirmCloseApplication = new CreateAlert().confirmCloseAllWindows();
        } else {
            confirmCloseApplication = new CreateAlert().confirmCloseApplication();
        }
        Optional<ButtonType> yesNoButton = confirmCloseApplication.showAndWait();
        if (yesNoButton.get() == confirmCloseApplication.getButtonTypes().get(Alerts.ButtonsType.YES_BUTTON.ordinal())) {
            if (messageBox.getMainWindowView().isAnyWindowOpen()) {
                new LogOutEvent().closeAllOpenedWindows();
            }
            new LogOutEvent().logout();
            LogInWindow.createOrGetInstance().close();

            messageBox.getStage().close();
            if (messageBox.getSystemTray() != null) {
                if (messageBox.getSystemTray().getTrayIcons().length > 0) {
                    messageBox.getSystemTray().remove(messageBox.getSystemTray().getTrayIcons()[0]);
                }
            }
            Platform.exit();
        } else {
            event.consume();
        }
    }
}
