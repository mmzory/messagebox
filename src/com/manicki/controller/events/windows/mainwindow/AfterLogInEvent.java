package com.manicki.controller.events.windows.mainwindow;

import com.manicki.controller.Logger;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.Messages;
import com.manicki.service.database.SqlQuerys;
import com.manicki.view.windows.mainwindow.components.menubar.Menus;
import com.manicki.view.windows.mainwindow.components.menubar.menuitems.FileMenuItems;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;

public class AfterLogInEvent {
    private MessageBox messageBox;

    public AfterLogInEvent() {
        this.messageBox = MessageBox.getInstance();
    }

    public void saveUsernameInConfigFile(String username) {
        messageBox.setUsername(username);
        try {
            messageBox.getConfiguration().getConfigFile().put("user", username);
            messageBox.getConfiguration().getConfigFile()
                    .store(new FileOutputStream(messageBox.getConfiguration().getConfigFilePath()),
                            "#serverIp = KINGSTAL-FTP\n#serverIp = SERWER_NAS");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void changeMainWindowLook() {
        try {
            messageBox.getStage().setTitle(messageBox.getLanguageFileProperty("version") +
                    messageBox.getLanguageFileProperty("loggedIn") +
                    messageBox.getUsername() + ")");
            messageBox.getMainWindowView().getMessagesTable().setDisable(false);
            messageBox.getMainWindowView().getMainWindowEvents().refreshTable(new SqlQuerys().getInboxMessages());
            messageBox.getMenubarItem(Menus.FILE.ordinal(), FileMenuItems.MESSAGES.ordinal()).setDisable(false);
            messageBox.getMenubarItem(Menus.FILE.ordinal(), FileMenuItems.LOGIN.ordinal())
                    .setText(messageBox.getLanguageFileProperty("logout"));
            messageBox.getMenubarItem(Menus.FILE.ordinal(), FileMenuItems.LOGIN.ordinal()).setId("LogOut");
            messageBox.getMainWindowView().getChoiceMessageBoxPane().getChoiceBox().getSelectionModel()
                    .select(messageBox.getLanguageFileProperty("inbox"));
            new ChoiceMessageBoxEvent().setChooseBoxDisable(false);
            messageBox.getMainWindowView().getMainWindowBottomPane().setButtonsDisabled(false);
            messageBox.getMainWindowView().getMainWindowEvents().setImageIconPath(messageBox.getStage(), true);
            Logger.getInstance().log("User signed in.");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void startThreadToCheckNewMessages() {
        new Messages().getMessagesEvents().runThreadToCheckNewMessages();
    }
}
