package com.manicki.controller.events.windows.mainwindow;

import com.manicki.messagebox.MessageBox;
import com.manicki.view.windows.mainwindow.components.HideApplicationPopupMenu;
import javafx.application.Platform;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class HideApplication {
    private MessageBox messageBox;
    private TrayIcon trayIcon;

    public HideApplication() {
        this.messageBox = MessageBox.getInstance();
    }

    public void hideMainWindow() {
        createTrayIcon();
        saveMainWindowLocation();
        saveMainWindowSize();
        messageBox.getMainWindowView().setHidden(true);
        Platform.runLater(() -> {
            messageBox.getMainWindowView().getMainPane().setCenter(null);
            messageBox.getStage().hide();
        });
    }

    public void createTrayIcon() {
        if (SystemTray.isSupported()) {
            try {
                messageBox.setSystemTray(SystemTray.getSystemTray());
                File file = new File(messageBox.getMainWindowView().getMainWindowEvents().getImageIconPath());
                Image trayImage = ImageIO.read(file);

                trayIcon = new TrayIcon(trayImage);
                trayIcon.setImageAutoSize(true);

                if (messageBox.getSystemTray().getTrayIcons().length > 0) {
                    messageBox.getSystemTray().remove(messageBox.getSystemTray().getTrayIcons()[0]);
                }
                messageBox.getSystemTray().add(trayIcon);

                trayIcon.displayMessage(
                        messageBox.getLanguageFileProperty("version"),
                        messageBox.getLanguageFileProperty("applicationHide"),
                        TrayIcon.MessageType.INFO);
                trayIcon.setToolTip(messageBox.getLanguageFileProperty("version"));

                addPopupMenu();
            } catch (IOException | AWTException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveMainWindowLocation() {
        messageBox.getMainWindowView()
                .setWindowLocation(new Point2D(messageBox.getStage().getX(), messageBox.getStage().getY()));
    }

    private void saveMainWindowSize() {
        messageBox.getMainWindowView()
                .setWindowSize(new Dimension2D(messageBox.getStage().getWidth(), messageBox.getStage().getHeight()));
    }

    private void addPopupMenu() {
        trayIcon.setPopupMenu(new HideApplicationPopupMenu().createMenu());
    }
}
