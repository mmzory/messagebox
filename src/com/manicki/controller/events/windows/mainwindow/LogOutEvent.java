package com.manicki.controller.events.windows.mainwindow;

import com.manicki.controller.Logger;
import com.manicki.controller.errorsandexceptions.Alerts;
import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.Messages;
import com.manicki.view.windows.mainwindow.components.menubar.Menus;
import com.manicki.view.windows.mainwindow.components.menubar.menuitems.FileMenuItems;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

public class LogOutEvent {
    private MessageBox messageBox;

    public LogOutEvent() {
        this.messageBox = MessageBox.getInstance();
    }

    private void disconnectDatabase() {
        if (messageBox.getDatabase() != null && messageBox.getDatabase().getConnection() != null) {
            new Messages().getMessagesEvents().stopThreadToCheckNewMessages();
            messageBox.getDatabase().getDatabaseConnection().disconnect();
        }
    }

    public void changeMainWindowLook() {
        messageBox.getStage().setTitle(messageBox.getLanguageFileProperty("version") +
                messageBox.getLanguageFileProperty("notLoggedIn"));
        messageBox.getMainWindowView().getMessagesTable()
                .setSelectedRow(messageBox.getMainWindowView().getMessagesTable().getSelectedRowIndex());
        messageBox.getMainWindowView().getMessagesTable().setDisable(true);
        messageBox.getMenubarItem(Menus.FILE.ordinal(), FileMenuItems.MESSAGES.ordinal()).setDisable(true);
        messageBox.getMenubarItem(Menus.FILE.ordinal(), FileMenuItems.LOGIN.ordinal())
                .setText(messageBox.getLanguageFileProperty("login"));
        messageBox.getMenubarItem(Menus.FILE.ordinal(), FileMenuItems.LOGIN.ordinal()).setId("LogIn");
        new ChoiceMessageBoxEvent().setChooseBoxDisable(true);
        messageBox.getMainWindowView().getMainWindowBottomPane().setButtonsDisabled(true);
        messageBox.getMainWindowView().getMainWindowEvents().setImageIconPath(messageBox.getStage(), false);
        if (messageBox.getMainWindowView().isHidden()) {
            new HideApplication().createTrayIcon();
        }
        Logger.getInstance().log("User signed out.");
    }

    public boolean confirmToCloseAllOpenedWindows() {
        boolean windowsClosed = false;
        if (messageBox.getMainWindowView().isAnyWindowOpen()) {
            Alert confirmCloseAllWindows = new CreateAlert().confirmCloseAllWindows();
            Optional<ButtonType> yesNoButton = confirmCloseAllWindows.showAndWait();
            if (yesNoButton.get() == confirmCloseAllWindows.getButtonTypes().get(Alerts.ButtonsType.YES_BUTTON.ordinal())) {
                closeConfirmedWindows();
                windowsClosed = true;
            }
        } else {
            windowsClosed = true;
        }
        return windowsClosed;
    }

    public void closeConfirmedWindows() {
        System.out.println("LogOutEvent.closeConfirmedWindows");
        for (int i = messageBox.getMainWindowView().getOpenedWindows().size(); i > 0; i--) {
            messageBox.getMainWindowView().getOpenedWindows().get(i - 1).close();
        }
    }

    public void closeAllOpenedWindows() {
        System.out.println("LogOutEvent.closeAllOpenedWindows");
        for (int i = messageBox.getMainWindowView().getAllOpenedWindows().size(); i > 0; i--) {
            messageBox.getMainWindowView().getAllOpenedWindows().get(i - 1).close();
        }
    }

    public void logout() {
        messageBox.setUsername("");
        changeMainWindowLook();
        disconnectDatabase();
    }
}
