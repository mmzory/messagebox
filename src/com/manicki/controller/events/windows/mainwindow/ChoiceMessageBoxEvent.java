package com.manicki.controller.events.windows.mainwindow;

import com.manicki.messagebox.MessageBox;
import com.manicki.service.database.SqlQuerys;
import com.manicki.view.windows.mainwindow.components.ChoiceMessageBoxPane;
import com.manicki.view.windows.mainwindow.components.messagestable.MessagesTableColumns;
import javafx.application.Platform;

import java.sql.SQLException;

public class ChoiceMessageBoxEvent {
    private MessageBox messageBox;

    public ChoiceMessageBoxEvent() {
        this.messageBox = MessageBox.getInstance();
    }

    public void setChooseBoxDisable(boolean disable) {
        messageBox.getMainWindowView().getChoiceMessageBoxPane().getChoiceBox().setDisable(disable);
    }

    public void chosenBox(ChoiceMessageBoxPane choiceMessageBoxPane) throws SQLException {
        if (choiceMessageBoxPane.getChoiceBox().getValue().equals(messageBox.getLanguageFileProperty("inbox"))) {
            messageBox.getMainWindowView().getMessagesTable()
                    .setTableColumnName(MessagesTableColumns.SENDER.ordinal(), messageBox.getLanguageFileProperty("sender"));
            messageBox.getMainWindowView().getMainWindowEvents().refreshTable(new SqlQuerys().getInboxMessages());
            choiceMessageBoxPane.setKeepField("");
        } else if (choiceMessageBoxPane.getChoiceBox().getValue().equals(messageBox.getLanguageFileProperty("outbox"))) {
            messageBox.getMainWindowView().getMessagesTable()
                    .setTableColumnName(MessagesTableColumns.SENDER.ordinal(), messageBox.getLanguageFileProperty("recipient"));
            messageBox.getMainWindowView().getMainWindowEvents().refreshTable(new SqlQuerys().getOutboxMessages());
            choiceMessageBoxPane.setKeepField("");
        } else {
            messageBox.getMainWindowView().getMessagesTable()
                    .setTableColumnName(MessagesTableColumns.SENDER.ordinal(), messageBox.getLanguageFileProperty("sender"));
        }
        setDeleteButtonBehaviour(choiceMessageBoxPane);
        removeTemporaryFields(choiceMessageBoxPane);
    }

    private void removeTemporaryFields(ChoiceMessageBoxPane choiceMessageBoxPane) {
        if (choiceMessageBoxPane != null) {
            Platform.runLater(choiceMessageBoxPane::removeTemporaryFields);
        }
    }

    private void setDeleteButtonBehaviour(ChoiceMessageBoxPane choiceMessageBoxPane) {
        String choiceBoxState = choiceMessageBoxPane.getChoiceBox().getValue().toString();

        if (choiceBoxState.equals(messageBox.getLanguageFileProperty("restoration"))) {
            messageBox.getMainWindowView().getMainWindowBottomPane().getDeleteButton().setText(messageBox.getLanguageFileProperty("restore"));
            messageBox.getMainWindowView().getMainWindowBottomPane().getDeleteButton().setId("RestoreMessage");
        } else {
            messageBox.getMainWindowView().getMainWindowBottomPane().getDeleteButton().setText(messageBox.getLanguageFileProperty("deleteMessage"));
            messageBox.getMainWindowView().getMainWindowBottomPane().getDeleteButton().setId("DeleteMessage");
        }
    }
}
