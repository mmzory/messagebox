package com.manicki.controller.events.windows.mainwindow;

import com.manicki.controller.events.windows.ShowMessageWindowEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.Messages;
import com.manicki.view.windows.ShowMessageWindow;
import com.manicki.view.windows.mainwindow.components.MessagesTable;
import javafx.scene.input.MouseEvent;

public class MessagesTableEvents {
    private MessageBox messageBox;

    public MessagesTableEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public void setTableMouseAction() {
        MessagesTable messagesTable = messageBox.getMainWindowView().getMessagesTable();
        messagesTable.setOnMouseClicked((MouseEvent event) -> {
            if (!messagesTable.getSelectionModel().isEmpty()) {
                if ((event.getClickCount() == 2) && (event.getButton() == event.getButton().PRIMARY)) {
                    new ShowMessageWindow().create();
                    if (messageBox.getMainWindowView().getChoiceMessageBoxPane().getChoiceBox().getValue().toString()
                            .equals(messageBox.getLanguageFileProperty("inbox"))) {
                        new ShowMessageWindowEvents().setMessageAsReaded(new Messages().getIdOfSelectedMessage());
                    }
                }
            }
        });
    }
}