package com.manicki.controller.events.windows.mainwindow.menubar.menuitems;

import com.manicki.view.windows.ShortcutKeysWindow;

public class HelpMenuEvents {

    public void event(String menuItemId) {
        switch (menuItemId) {
            case "shortcutKeys":
                ShortcutKeysWindow.getInstance();
                break;
        }
    }
}
