package com.manicki.controller.events.windows.mainwindow.menubar.menuitems;

import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.controller.events.MessagesEvents;
import com.manicki.controller.events.windows.ShowMessageWindowEvents;
import com.manicki.controller.events.windows.mainwindow.CloseApplication;
import com.manicki.controller.events.windows.mainwindow.HideApplication;
import com.manicki.controller.events.windows.mainwindow.LogOutEvent;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.Messages;
import com.manicki.service.database.SqlQuerys;
import com.manicki.view.Windows;
import com.manicki.view.windows.CreateMessageWindow;
import com.manicki.view.windows.LogInWindow;
import com.manicki.view.windows.SearchMessageWindow;
import com.manicki.view.windows.ShowMessageWindow;
import com.manicki.view.windows.mainwindow.components.ChoiceMessageBoxPane;
import javafx.scene.control.Alert;
import javafx.stage.WindowEvent;

import java.sql.SQLException;

public class FileMenuEvents {
    private MessageBox messageBox;
    private MessagesEvents messagesEvents = new MessagesEvents();

    public FileMenuEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public void event(String menuItemId) {
        switch (menuItemId) {
            case "LogIn":
                logIn();
                break;
            case "LogOut":
                logOut();
                break;
            case "NewMessage":
                new CreateMessageWindow().create(null, "none", null);
                break;
            case "ShowMessage":
                showMessage();
                break;
            case "DeleteMessage":
                messagesEvents.deleteMessage();
                break;
            case "Restore":
                restoreMessageView();
                break;
            case "RestoreMessage":
                messagesEvents.restoreMessage();
                break;
            case "Find":
                SearchMessageWindow.getInstance();
                break;
            case "Hide":
                new HideApplication().hideMainWindow();
                break;
            case "Close":
                new CloseApplication().showConfirmation(new WindowEvent(messageBox.getStage(),
                        WindowEvent.WINDOW_CLOSE_REQUEST));
                break;
        }
    }

    private void logIn() {
        Windows logInWindow = LogInWindow.createOrGetInstance();
        ((LogInWindow) logInWindow).create();
    }

    private void logOut() {
        boolean allWindowsClosed = new LogOutEvent().confirmToCloseAllOpenedWindows();
        if (allWindowsClosed) {
            new LogOutEvent().logout();
        }
    }

    private void showMessage() {
        if (messageBox.getMainWindowView().getMessagesTable().getSelectedRowIndex() >= 0) {
            new ShowMessageWindow().create();
            new ShowMessageWindowEvents().setMessageAsReaded(new Messages().getIdOfSelectedMessage());
        } else {
            Alert messageNotSelected = new CreateAlert().messageNotSelected();
            messageNotSelected.show();
        }
    }

    private void restoreMessageView() {
        try {
            ChoiceMessageBoxPane choiceMessageBoxPane = messageBox.getMainWindowView().getChoiceMessageBoxPane();
            choiceMessageBoxPane.setKeepField(messageBox.getLanguageFileProperty("restoration"));
            if (!choiceMessageBoxPane.getChoiceBox().getItems().contains(messageBox.getLanguageFileProperty("restoration"))) {
                choiceMessageBoxPane.addField(messageBox.getLanguageFileProperty("restoration"));
            }
            messageBox.getMainWindowView().getMainWindowEvents().refreshTable(new SqlQuerys().getDeletedMessages());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
