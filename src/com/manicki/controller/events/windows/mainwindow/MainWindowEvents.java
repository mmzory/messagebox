package com.manicki.controller.events.windows.mainwindow;

import com.manicki.controller.Logger;
import com.manicki.controller.events.ShortcutKeys;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.Windows;
import com.manicki.view.windows.mainwindow.components.MessagesTable;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;


public class MainWindowEvents {
    private MessageBox messageBox;
    private String imageIconPath = "images/mainWindowIcon_disconnected.gif";
    private boolean messagesTableCreated = false;
    private Image imageIcon;

    public MainWindowEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public void setWindowLocationAndSize() {
        if ((Toolkit.getDefaultToolkit().getScreenSize().getWidth() < 800) &&
                (Toolkit.getDefaultToolkit().getScreenSize().getHeight() < 600)) {
            messageBox.getStage().setWidth(Toolkit.getDefaultToolkit().getScreenSize().getWidth() * 3 / 4);
            messageBox.getStage().setHeight(Toolkit.getDefaultToolkit().getScreenSize().getHeight() * 3 / 4);
            messageBox.getStage().setFullScreen(true);
        } else {
            messageBox.getStage().setWidth(800);
            messageBox.getStage().setHeight(600);
        }
    }

    public void setWindowIcon(Stage stage) {
        try {
            imageIcon = new Image(new FileInputStream(imageIconPath));
            stage.getIcons().add(imageIcon);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Logger.getInstance().logError("Image file not found! "
                    + System.lineSeparator() + e.getMessage()
            );
        }
    }

    public String getImageIconPath() {
        return imageIconPath;
    }

    public void refreshTable(ResultSet resultSet) {
        if (!messagesTableCreated) {
            getMessagesTable().create();
            messagesTableCreated = true;
        }
        getMessagesTable().fillContent(resultSet);
        getMessagesTable().getSelectionModel().select(getMessagesTable().getSelectedRow());
    }

    public void setMainWindowShortcutKeys(Scene scene) {
        ShortcutKeys shortcutKeys = ShortcutKeys.getInstance();
        shortcutKeys.createNewMessageShortcut(scene);
        shortcutKeys.changeBoxShortcut(scene);
        shortcutKeys.deleteMessageShortcut(scene);
        shortcutKeys.openFindMessageWindow(scene);
        shortcutKeys.openMessageShortcut(scene);
        shortcutKeys.quitApplicationShortcut(scene);
        shortcutKeys.signInOrOutShortcut(scene);
    }

    public void setImageIconPath(Stage stage, boolean connected) {
        stage.getIcons().removeAll(stage.getIcons());
        if (connected) {
            imageIconPath = "images/mainWindowIcon.gif";
        } else {
            imageIconPath = "images/mainWindowIcon_disconnected.gif";
        }
        setWindowIcon(stage);

        for (Windows openedWindow : messageBox.getMainWindowView().getAllOpenedWindows()) {
            openedWindow.getStage().getIcons().removeAll(openedWindow.getStage().getIcons());
            setWindowIcon(openedWindow.getStage());
        }
    }

    private MessagesTable getMessagesTable() {
        return messageBox.getMainWindowView().getMessagesTable();
    }
}
