package com.manicki.controller.events.windows;

import com.manicki.messagebox.MessageBox;
import com.manicki.view.Windows;
import javafx.stage.Stage;

public class DefaultWindowsEvents {
    private MessageBox messageBox;

    public DefaultWindowsEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public void setWindowLocationAndSize(Stage stage) {
        stage.setX(messageBox.getStage().getX() + (Windows.WINDOW_OFFSET));
        stage.setY(messageBox.getStage().getY() + (Windows.WINDOW_OFFSET));
        stage.setWidth(messageBox.getStage().getWidth());
        stage.setHeight(messageBox.getStage().getHeight());
    }

    public void setWindowLocationInCenterOfMainWindowAndShow(Stage stage) {
        stage.setOpacity(0f);
        stage.show();
        stage.setX(messageBox.getStage().getX() + (messageBox.getStage().getWidth() / 2) - (stage.getWidth() / 2));
        stage.setY(messageBox.getStage().getY() + (messageBox.getStage().getHeight() / 2) - (stage.getHeight() / 2));
        stage.setOpacity(1f);
    }
}
