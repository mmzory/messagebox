package com.manicki.controller.events.windows;

import com.manicki.controller.events.ShortcutKeys;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.database.SqlQuerys;
import com.manicki.view.windows.mainwindow.components.ChoiceMessageBoxPane;
import com.manicki.view.windows.mainwindow.components.messagestable.MessagesTableColumns;
import javafx.scene.Scene;
import javafx.scene.control.Button;

import java.sql.SQLException;

public class SearchMessageWindowEvents {
    private MessageBox messageBox;

    public SearchMessageWindowEvents() {
        this.messageBox = MessageBox.getInstance();
    }

    public void event(String buttonId, String boxType, String searchingIn, String searchingText, boolean includeDeleted) {
        switch (buttonId) {
            case "search":
                foundMessage(boxType, searchingIn, searchingText, includeDeleted);
                break;
        }
    }

    public void setShortcutKeys(Scene scene, Button acceptButton, Button cancelButton) {
        ShortcutKeys shortcutKeys = ShortcutKeys.getInstance();
        shortcutKeys.pushAcceptOrCancelButtonShortcut(scene, acceptButton, cancelButton);
    }

    private void foundMessage(String boxType, String searchingIn, String searchingText, boolean includeDeleted) {
        try {
            ChoiceMessageBoxPane choiceMessageBoxPane = messageBox.getMainWindowView().getChoiceMessageBoxPane();
            choiceMessageBoxPane.setKeepField(messageBox.getLanguageFileProperty("found_message"));
            choiceMessageBoxPane.setBoxType(boxType);
            if (!choiceMessageBoxPane.getChoiceBox().getItems().contains(messageBox.getLanguageFileProperty("found_message"))) {
                choiceMessageBoxPane.addField(messageBox.getLanguageFileProperty("found_message"));
            }
            if (boxType.equals(messageBox.getLanguageFileProperty("outbox"))) {
                messageBox.getMainWindowView().getMessagesTable().setTableColumnName(MessagesTableColumns.SENDER.ordinal(),
                        messageBox.getLanguageFileProperty("recipient"));
            } else {
                messageBox.getMainWindowView().getMessagesTable().setTableColumnName(MessagesTableColumns.SENDER.ordinal(),
                        messageBox.getLanguageFileProperty("sender"));
            }
            messageBox.getMainWindowView().getMainWindowEvents()
                    .refreshTable(new SqlQuerys().findMessage(boxType, searchingIn, searchingText, includeDeleted));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
