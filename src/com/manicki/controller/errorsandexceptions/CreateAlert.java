package com.manicki.controller.errorsandexceptions;

import com.manicki.messagebox.MessageBox;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class CreateAlert {
    private MessageBox messageBox;
    private Alert alert;

    public CreateAlert() {
        this.messageBox = MessageBox.getInstance();
    }

    public Alert loadingConfigurationFileError() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.ERROR)
                .setAlertTitle("Critical error!")
                .setAlertMessage("Loading config file error!" + "\n\n" +
                        "Closing application...")
                .isYesNoAlert(false)
                .biuld()
                .getAlert(messageBox, true, null);

        return alert;
    }

    public Alert creatingDatabaseTableError() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.ERROR)
                .setAlertTitle(messageBox.getLanguageFileProperty("error"))
                .setAlertMessage(messageBox.getLanguageFileProperty("creatingDatabaseTableError"))
                .isYesNoAlert(false)
                .biuld().getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert connectionErrorOrIncorrectData() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.ERROR)
                .setAlertTitle(messageBox.getLanguageFileProperty("error"))
                .setAlertMessage(messageBox.getLanguageFileProperty("connectionError") + " " +
                        messageBox.getLanguageFileProperty("or") + "\n" +
                        messageBox.getLanguageFileProperty("incorrectData"))
                .isYesNoAlert(false)
                .biuld()
                .getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert usernameFieldIsEmpty() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.ERROR)
                .setAlertTitle(messageBox.getLanguageFileProperty("error"))
                .setAlertMessage(messageBox.getLanguageFileProperty("usernameFieldIsEmpty"))
                .isYesNoAlert(false)
                .biuld().getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert passwordFieldIsEmpty() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.ERROR)
                .setAlertTitle(messageBox.getLanguageFileProperty("error"))
                .setAlertMessage(messageBox.getLanguageFileProperty("passwordFieldIsEmpty"))
                .isYesNoAlert(false)
                .biuld().getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert messageNotSelected() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.WARNING)
                .setAlertTitle(messageBox.getLanguageFileProperty("warning"))
                .setAlertMessage(messageBox.getLanguageFileProperty("messageNotSelected") + "\n" +
                        messageBox.getLanguageFileProperty("chooseMessageAndTryAgain"))
                .isYesNoAlert(false)
                .biuld()
                .getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert confirmCloseAllWindows() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.WARNING)
                .setAlertTitle(messageBox.getLanguageFileProperty("warning"))
                .setAlertMessage(messageBox.getLanguageFileProperty("youHaveOpenedWindows") + "\n" +
                        messageBox.getLanguageFileProperty("doYouWishToContinue"))
                .isYesNoAlert(true)
                .biuld().getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert confirmCloseApplication() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.CONFIRMATION)
                .setAlertTitle(messageBox.getLanguageFileProperty("confirm"))
                .setAlertMessage(messageBox.getLanguageFileProperty("areYouSureYouWantCloseApp"))
                .isYesNoAlert(true)
                .biuld().getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert recipientTopicOrMessageFieldIsEmpty(String errorMessage) {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.ERROR)
                .setAlertTitle(messageBox.getLanguageFileProperty("error"))
                .setAlertMessage(errorMessage)
                .isYesNoAlert(false)
                .biuld()
                .getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert messageFieldIsNotEmpty(Stage owner) {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.WARNING)
                .setAlertTitle(messageBox.getLanguageFileProperty("warning"))
                .setAlertMessage(messageBox.getLanguageFileProperty("messageFieldIsNotEmpty") + "\n" +
                        messageBox.getLanguageFileProperty("doYouWishToContinue"))
                .isYesNoAlert(true)
                .biuld().getAlert(messageBox, true, owner);

        return alert;
    }

    public Alert databaseConnectionBroken() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.ERROR)
                .setAlertTitle(messageBox.getLanguageFileProperty("error"))
                .setAlertMessage(messageBox.getLanguageFileProperty("connectionError") + "!\n"
                        + messageBox.getLanguageFileProperty("userLoggedOut"))
                .isYesNoAlert(false)
                .biuld()
                .getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert newMessage() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.INFORMATION)
                .setAlertTitle(messageBox.getLanguageFileProperty("information"))
                .setAlertMessage(messageBox.getLanguageFileProperty("newMessage") + "!")
                .isYesNoAlert(false)
                .biuld()
                .getAlert(messageBox, true, null);

        return alert;
    }

    public Alert confirmToDeleteMessage() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.CONFIRMATION)
                .setAlertTitle(messageBox.getLanguageFileProperty("confirm"))
                .setAlertMessage(messageBox.getLanguageFileProperty("areYouSureYouWantDeleteMessage"))
                .isYesNoAlert(true)
                .biuld().getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }

    public Alert errorDeletingMessage() {
        alert = new Alerts.AlertsBuilder(Alert.AlertType.ERROR)
                .setAlertTitle(messageBox.getLanguageFileProperty("error"))
                .setAlertMessage(messageBox.getLanguageFileProperty("errorDeletingMessage") + "\n"
                + messageBox.getLanguageFileProperty("possibilityOfDeletingMessages"))
                .isYesNoAlert(false)
                .biuld()
                .getAlert(messageBox, true, messageBox.getStage());

        return alert;
    }
}
