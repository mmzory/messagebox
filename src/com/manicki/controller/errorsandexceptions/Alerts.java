package com.manicki.controller.errorsandexceptions;

import com.manicki.messagebox.MessageBox;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Alerts {
    private MessageBox messageBox = MessageBox.getInstance();
    private Alert.AlertType alertType;
    private String alertTitle;
    private String alertMessage;
    private boolean isShowAndWaitAlert;
    private ButtonType yes = new ButtonType(messageBox.getLanguageFileProperty("yes"), ButtonBar.ButtonData.YES);
    private ButtonType no = new ButtonType(messageBox.getLanguageFileProperty("no"), ButtonBar.ButtonData.NO);

    private Alerts(AlertsBuilder alertsBuilder) {
        this.alertType = alertsBuilder.alertType;
        this.alertTitle = alertsBuilder.alertTitle;
        this.alertMessage = alertsBuilder.alertMessage;
        this.isShowAndWaitAlert = alertsBuilder.isShowAndWait;
    }

    public Alert getAlert(MessageBox messageBox, boolean noHeaderAlert, Stage owner) {
        Alert alert = isShowAndWaitAlert ?
                new Alert(this.alertType, this.alertMessage, yes, no) :
                new Alert(this.alertType, this.alertMessage, new ButtonType("OK"));

        if (noHeaderAlert) {
            alert.setHeaderText(null);
        } else {
            alert.setHeaderText(alertTitle);
        }

        alert.setTitle(this.alertTitle);
        if (messageBox != null) {
            alert.initOwner(owner);
        }

        alert.initModality(Modality.APPLICATION_MODAL);

        return alert;
    }

    public static class AlertsBuilder {
        private Alert.AlertType alertType;
        private String alertTitle;
        private String alertMessage;
        private boolean isShowAndWait;

        public AlertsBuilder(Alert.AlertType alertType) {
            this.alertType = alertType;
        }

        public AlertsBuilder setAlertTitle(String alertTitle) {
            this.alertTitle = alertTitle;
            return this;
        }

        public AlertsBuilder setAlertMessage(String alertMessage) {
            this.alertMessage = alertMessage;
            return this;
        }

        public AlertsBuilder isYesNoAlert(boolean isShowAndWait) {
            this.isShowAndWait = isShowAndWait;
            return this;
        }

        public Alerts biuld() {
            return new Alerts(this);
        }
    }

    public enum ButtonsType {
        YES_BUTTON,
        NO_BUTTON;
    }
}
