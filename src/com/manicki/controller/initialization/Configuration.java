package com.manicki.controller.initialization;

import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.controller.errorsandexceptions.ErrorStatus;
import javafx.application.Platform;
import javafx.scene.control.Alert;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Properties;

public class Configuration {
    private final String configFilePath = "config\\config.properties";
    private StringBuilder languageFilePath = new StringBuilder("language\\");
    private Properties configFile;
    private Properties languageFile;

    public void loadSettingsAndLanguageFile() {
        configFile = new Properties();
        languageFile = new Properties();
        try {
            configFile.load(new FileInputStream(configFilePath));
            loadFilesInChosenLanguage(configFile.getProperty("lang"));
        } catch (IOException e) {
            Alert alert = new CreateAlert().loadingConfigurationFileError();
            alert.showAndWait();
            Platform.exit();
            System.exit(ErrorStatus.CONFIGURATION_FILE_NOT_FOUND.ordinal());
        }
    }

    private void loadFilesInChosenLanguage(String language) throws IOException {
        Charset charset;

        switch (language) {
            case "polish":
                languageFilePath.append("polishLang.properties");
                charset = Charset.forName(LanguageCharsets.POLISH.getCharsetName());
                break;
            default:
                languageFilePath.append("englishLang.properties");
                charset = Charset.forName(LanguageCharsets.ENGLISH.getCharsetName());
        }
        languageFile.load(new InputStreamReader(
                new FileInputStream(
                        languageFilePath.toString()
                ), charset
        ));
    }

    public Properties getConfigFile() {
        return configFile;
    }

    public String getConfigFilePath() {
        return configFilePath;
    }

    public Properties getLanguageFile() {
        return languageFile;
    }
}
