package com.manicki.controller.initialization;

public enum LanguageCharsets {
    ENGLISH("UTF-8"),
    POLISH("windows-1250");

    private String charsetName;

    LanguageCharsets(String charsetName) {
        this.charsetName = charsetName;
    }

    public String getCharsetName() {
        return charsetName;
    }
}

//TODO get language files from 'language' folder, read and set name from filename. Use charset from language file.