package com.manicki.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Logger {
    private static Logger instance;
    private String logFolder;

    private Logger() {
        logFolder = "logs\\";
    }

    public static Logger getInstance(){
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    public void log(String message) {
        try {
            String fileName = "log_" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd")) + ".log";
            FileOutputStream fileOutputStream = new FileOutputStream(logFolder + fileName, true);
            writeDateAndTime(fileOutputStream);
            writeMessage(message, fileOutputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void logError(String errorMessage) {
        try {
            String fileName = "error_log_" + LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy.MM.dd")) + ".log";
            FileOutputStream fileOutputStream = new FileOutputStream(logFolder + fileName, true);
            writeDateAndTime(fileOutputStream);
            writeMessage(errorMessage, fileOutputStream);
            writeClassName(fileOutputStream);
            fileOutputStream.write(System.lineSeparator().getBytes());
            fileOutputStream.write(System.lineSeparator().getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeDateAndTime(FileOutputStream fileOutputStream) throws IOException {
        fileOutputStream.write("Date: ".getBytes());
        fileOutputStream.write(LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy \t")).getBytes());
        fileOutputStream.write("Time: ".getBytes());
        fileOutputStream.write(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")).getBytes());
        fileOutputStream.write(System.lineSeparator().getBytes());
    }

    private void writeMessage(String message, FileOutputStream fileOutputStream) throws IOException {
        fileOutputStream.write(message.getBytes());
        fileOutputStream.write(System.lineSeparator().getBytes());
        fileOutputStream.write(System.lineSeparator().getBytes());
    }

    private void writeClassName(FileOutputStream fileOutputStream) throws IOException {
        StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
        String stackTrace = new String();
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            stackTrace += stackTraceElement.getClassName() + " "
                    + stackTraceElement.getMethodName() + " "
                    + stackTraceElement.getLineNumber()
                    + System.lineSeparator()
            ;
        }
        fileOutputStream.write(stackTrace.getBytes());
    }
}
