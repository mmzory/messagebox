package com.manicki.view;

import com.manicki.messagebox.MessageBox;
import javafx.stage.Stage;

public interface Windows {
    int WINDOW_OFFSET = 20;
    double SPACING = 10;
    double MIN_WIDTH_SIZE = 120;

    default void setStageTitle(Stage stage, String title) {
        stage.setTitle(MessageBox.getInstance().getLanguageFileProperty("version") + "- "
                + title);
    }

    default void setWindowIcon(Stage stage) {
        MessageBox.getInstance().getMainWindowView().getMainWindowEvents().setWindowIcon(stage);
    };

    Stage getStage();
    void close();
}
