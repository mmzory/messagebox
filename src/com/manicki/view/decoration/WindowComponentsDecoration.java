package com.manicki.view.decoration;

public class WindowComponentsDecoration {
    public String setSoftTableViewDecoration() {
        String decoration = ""
                + "-fx-focus-color: transparent;\n";
//                        + "-fx-faint-focus-color: transparent;";
        return decoration;
    }

    public String setTextColorOnRed() {
        return "-fx-text-fill: red;\n";
    }

    public String setTextColorOnBlack() {
        return "-fx-text-fill: black;\n";
    }

    public String setFontSize(int size) {
        return "-fx-font-size: " + size + ";\n";
    }

    public String setBackgroundColor(String color) {
        return "-fx-background-color: " + color + ";";
    }

    public String getTabPaneDecoration() {
        return  "-fx-border-color: #aaaaaa;\n" +
                "-fx-border-width: 0px 1px 1px 1px;\n" +
                "-fx-border-radius: 0 0 5 5;\n" +
                "-fx-background-radius: 0 0 5 5;";
    }
}
