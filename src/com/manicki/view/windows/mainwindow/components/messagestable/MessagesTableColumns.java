package com.manicki.view.windows.mainwindow.components.messagestable;

public enum MessagesTableColumns {
    ID,
    DATE,
    SENDER,
    TOPIC,
    MESSAGE,
    COMPUTER_NAME,
    RECIPIENT,
    READED,
    DELETED;
}
