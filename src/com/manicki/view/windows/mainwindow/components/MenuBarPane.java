package com.manicki.view.windows.mainwindow.components;

import com.manicki.messagebox.MessageBox;
import com.manicki.view.windows.mainwindow.components.menubar.EditMenu;
import com.manicki.view.windows.mainwindow.components.menubar.FileMenu;
import com.manicki.view.windows.mainwindow.components.menubar.HelpMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class MenuBarPane {
    private MessageBox messageBox;
    private MenuBar menuBar;
    private Menu messagesMenu, editMenu, viewMenu;
    private FileMenu fileMenu;
    private HelpMenu helpMenu;
    private MenuItem loginToDatabase, newMessage, showMessage, deleteMessage, hideWindow, closeApplication,
            refreshTable, findMessage, recoverMessage, options,
            usersOnline,
            help, whatsNew, author;

    public MenuBarPane() {
        this.messageBox = MessageBox.getInstance();
    }

    public MenuBar createContent() {
        fileMenu = new FileMenu().createContent();
        helpMenu = new HelpMenu().createContent();
        menuBar = new MenuBar(
                fileMenu,
                new EditMenu().cretateContent(),
                helpMenu
        );
        return this.menuBar;
    }

    public FileMenu getFileMenu() {
        return fileMenu;
    }
}
