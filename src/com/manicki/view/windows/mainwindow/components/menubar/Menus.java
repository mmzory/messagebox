package com.manicki.view.windows.mainwindow.components.menubar;

public enum Menus {
    FILE,
    EDIT,
    VIEW,
    HELP;
}
