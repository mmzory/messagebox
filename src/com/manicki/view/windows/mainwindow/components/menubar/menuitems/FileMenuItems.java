package com.manicki.view.windows.mainwindow.components.menubar.menuitems;

public enum FileMenuItems {
    LOGIN,
    SEPARATOR_MESSAGES,
    MESSAGES,
    SEPARATOR_HIDE_OR_CLOSE,
    HIDE,
    CLOSE;
}
