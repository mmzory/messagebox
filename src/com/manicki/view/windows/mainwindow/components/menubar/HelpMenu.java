package com.manicki.view.windows.mainwindow.components.menubar;

import com.manicki.controller.events.windows.mainwindow.menubar.menuitems.HelpMenuEvents;
import com.manicki.messagebox.MessageBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;

public class HelpMenu extends Menu {
    private MessageBox messageBox;

    private MenuItem shortcutKeys;

    public HelpMenu() {
        super();
        this.messageBox = MessageBox.getInstance();
    }

    public HelpMenu createContent() {
        this.setText(messageBox.getLanguageFileProperty("help"));

        shortcutKeys = new MenuItem(messageBox.getLanguageFileProperty("shortcutKeys"));
        shortcutKeys.setOnAction(event -> {
            new HelpMenuEvents().event("shortcutKeys");
        });

        this.getItems().add(shortcutKeys);

        return this;
    }
}
