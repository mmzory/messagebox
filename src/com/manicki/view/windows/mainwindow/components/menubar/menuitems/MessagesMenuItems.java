package com.manicki.view.windows.mainwindow.components.menubar.menuitems;

public enum MessagesMenuItems {
    NEWMESSAGE,
    SHOWMESSAGE,
    DELETEMESSAGE,
    SEPARATOR,
    RESTORE,
    FIND;
}
