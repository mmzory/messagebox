package com.manicki.view.windows.mainwindow.components.menubar;

import com.manicki.controller.events.windows.mainwindow.menubar.menuitems.FileMenuEvents;
import com.manicki.messagebox.MessageBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

public class FileMenu extends Menu {
    private MessageBox messageBox;
    private FileMenuEvents fileMenuEvents;
    private Menu messagesSubmenu;
    private MenuItem loginToDatabase,
            newMessage, showMessage, deleteMessage, restoreMessage, findMessage,
            hideWindow, closeApplication;

    public FileMenu() {
        super();
        this.messageBox = MessageBox.getInstance();
    }

    public FileMenu createContent() {
        fileMenuEvents = new FileMenuEvents();

        loginToDatabase = new MenuItem(messageBox.getLanguageFileProperty("login"));
        loginToDatabase.setId("LogIn");
        loginToDatabase.setOnAction(e -> fileMenuEvents.event(loginToDatabase.getId()));

        newMessage = new MenuItem(messageBox.getLanguageFileProperty("newMessage"));
        newMessage.setOnAction(event -> fileMenuEvents.event("NewMessage"));

        showMessage = new MenuItem(messageBox.getLanguageFileProperty("showMessage"));
        showMessage.setOnAction(event -> fileMenuEvents.event("ShowMessage"));

        deleteMessage = new MenuItem(messageBox.getLanguageFileProperty("deleteMessage"));
        deleteMessage.setOnAction(event -> fileMenuEvents.event("DeleteMessage"));

        restoreMessage = new MenuItem(messageBox.getLanguageFileProperty("restore") + " "
                + messageBox.getLanguageFileProperty("message").toLowerCase());
        restoreMessage.setOnAction(event -> fileMenuEvents.event("Restore"));

        findMessage = new MenuItem(messageBox.getLanguageFileProperty("find") + " "
                + messageBox.getLanguageFileProperty("message").toLowerCase());
        findMessage.setOnAction(event -> fileMenuEvents.event("Find"));

        messagesSubmenu = new Menu(messageBox.getLanguageFileProperty("messages"),
                null,
                newMessage,
                showMessage,
                deleteMessage,
                new SeparatorMenuItem(),
                restoreMessage,
                findMessage);
        messagesSubmenu.setDisable(true);

        hideWindow = new MenuItem(messageBox.getLanguageFileProperty("hideWindow"));
        hideWindow.setOnAction(event -> fileMenuEvents.event("Hide"));

        closeApplication = new MenuItem(messageBox.getLanguageFileProperty("close"));
        closeApplication.setOnAction(event -> fileMenuEvents.event("Close"));

        this.setText(messageBox.getLanguageFileProperty("file"));

        this.getItems().addAll(
                loginToDatabase,
                new SeparatorMenuItem(),
                messagesSubmenu,
                new SeparatorMenuItem(),
                hideWindow,
                closeApplication);

        return this;
    }

    public FileMenuEvents getFileMenuEvents() {
        return fileMenuEvents;
    }
}
