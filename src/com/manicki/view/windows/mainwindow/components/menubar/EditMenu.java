package com.manicki.view.windows.mainwindow.components.menubar;

import com.manicki.controller.events.windows.mainwindow.menubar.menuitems.EditMenuEvents;
import com.manicki.messagebox.MessageBox;
import javafx.scene.control.Menu;

public class EditMenu extends Menu {
    private MessageBox messageBox;
    private EditMenuEvents editMenuEvents;

    public EditMenu() {
        super();
        this.messageBox = MessageBox.getInstance();
    }

    public EditMenu cretateContent() {
        editMenuEvents = new EditMenuEvents();

        this.setText(messageBox.getLanguageFileProperty("edit"));

        this.getItems().addAll(
        );

        return this;
    }
}
