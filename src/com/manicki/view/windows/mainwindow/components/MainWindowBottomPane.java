package com.manicki.view.windows.mainwindow.components;

import com.manicki.messagebox.MessageBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

public class MainWindowBottomPane {
    private MessageBox messageBox;
    private Button bShowMessage, bNewMessage, bDeleteMessage;

    private final double PREF_WIDTH = 120, PREF_HEIGHT = 20, SPACING = 50;

    public MainWindowBottomPane() {
        this.messageBox = MessageBox.getInstance();
    }

    public HBox createPane() {
        HBox buttonsPane = new HBox();
        bShowMessage = new Button(messageBox.getLanguageFileProperty("showMessage"));
        bShowMessage.setPrefSize(PREF_WIDTH, PREF_HEIGHT);
        bShowMessage.setOnAction(action -> {
            messageBox.getMainWindowView().getMenuBarPane().getFileMenu().getFileMenuEvents()
                    .event("ShowMessage");
        });

        bNewMessage = new Button(messageBox.getLanguageFileProperty("newMessage"));
        bNewMessage.setPrefSize(PREF_WIDTH, PREF_HEIGHT);
        bNewMessage.setOnAction(action -> {
            messageBox.getMainWindowView().getMenuBarPane().getFileMenu().getFileMenuEvents()
                    .event("NewMessage");
        });

        bDeleteMessage = new Button(messageBox.getLanguageFileProperty("deleteMessage"));
        bDeleteMessage.setPrefSize(PREF_WIDTH, PREF_HEIGHT);
        bDeleteMessage.setId("DeleteMessage");
        bDeleteMessage.setOnAction(action -> {
            messageBox.getMainWindowView().getMenuBarPane().getFileMenu().getFileMenuEvents()
                    .event(bDeleteMessage.getId());
        });

        buttonsPane.getChildren().addAll(bShowMessage, bNewMessage, bDeleteMessage);

        buttonsPane.setPadding(new Insets(SPACING / ((SPACING / 10) + 1)));

        buttonsPane.setAlignment(Pos.CENTER);
        buttonsPane.setSpacing(SPACING);

        setButtonsDisabled(true);

        return buttonsPane;
    }

    public void setButtonsDisabled(boolean disabled) {
        bNewMessage.setDisable(disabled);
        bShowMessage.setDisable(disabled);
        bDeleteMessage.setDisable(disabled);
    }

    public Button getDeleteButton() {
        return bDeleteMessage;
    }
}
