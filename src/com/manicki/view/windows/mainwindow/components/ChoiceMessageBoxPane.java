package com.manicki.view.windows.mainwindow.components;

import com.manicki.controller.events.windows.mainwindow.ChoiceMessageBoxEvent;
import com.manicki.messagebox.MessageBox;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.sql.SQLException;

public class ChoiceMessageBoxPane {
    private MessageBox messageBox;
    private ChoiceMessageBoxEvent chooseMessageBoxEvent;
    private ComboBox choiceBox;
    private final int SPACING = 20;
    private String boxType = new String();
    private String keepField = new String();

    public ChoiceMessageBoxPane() {
        this.messageBox = MessageBox.getInstance();
    }

    public HBox createPane() {
        chooseMessageBoxEvent = new ChoiceMessageBoxEvent();

        Label lBox = new Label(messageBox.getLanguageFileProperty("box") + ": ");

        this.choiceBox = new ComboBox();
        addStandardChoiceMessageBoxChoicesFields();
        this.choiceBox.setValue(this.choiceBox.getItems().get(0));
        this.choiceBox.setDisable(true);
        this.choiceBox.setVisibleRowCount(Integer.MAX_VALUE);
        this.choiceBox.setOnAction(selectMessage -> {
            try {
                if (!this.choiceBox.getItems().isEmpty()) {
                    if (this.choiceBox.getValue().toString().equals(messageBox.getLanguageFileProperty("inbox")) ||
                            this.choiceBox.getValue().toString().equals(messageBox.getLanguageFileProperty("outbox"))) {
                        setBoxType(this.choiceBox.getValue().toString());
                    }
                    chooseMessageBoxEvent.chosenBox(this);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        HBox choiceBoxPane = new HBox(lBox, this.choiceBox);
        choiceBoxPane.setSpacing(SPACING);
        choiceBoxPane.setPadding(new Insets(SPACING / 3, SPACING, SPACING / 3, SPACING));
        choiceBoxPane.setAlignment(Pos.CENTER_LEFT);

        return choiceBoxPane;
    }

    public ComboBox getChoiceBox() {
        return this.choiceBox;
    }

    public void addField(String field) {
        this.choiceBox.getItems().add(field);
        this.choiceBox.getSelectionModel().select(field);
    }

    public void removeTemporaryFields() {
        if (this.choiceBox.getItems().contains(messageBox.getLanguageFileProperty("restoration")) &&
                !getKeepField().equals(messageBox.getLanguageFileProperty("restoration"))) {
            this.choiceBox.getItems().remove(messageBox.getLanguageFileProperty("restoration"));
        }
        if (this.choiceBox.getItems().contains(messageBox.getLanguageFileProperty("found_message")) &&
                !getKeepField().equals(messageBox.getLanguageFileProperty("found_message"))) {
            this.choiceBox.getItems().remove(messageBox.getLanguageFileProperty("found_message"));
        }
    }

    public String getChoiceBoxSelectedItem() {
        return getChoiceBox().getItems().get(getChoiceBox().getSelectionModel()
                .getSelectedIndex()).toString();
    }

    public String getBoxType() {
        return this.boxType;
    }

    public void setBoxType(String boxType) {
        this.boxType = boxType;
    }

    public String getKeepField() {
        return keepField;
    }

    public void setKeepField(String keepField) {
        this.keepField = keepField;
    }

    private void addStandardChoiceMessageBoxChoicesFields() {
        this.choiceBox.getItems().add(messageBox.getLanguageFileProperty("inbox"));
        this.choiceBox.getItems().add(messageBox.getLanguageFileProperty("outbox"));
    }
}
