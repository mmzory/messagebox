package com.manicki.view.windows.mainwindow.components;

import com.manicki.controller.events.windows.mainwindow.HideApplicationPopupMenuEvents;
import com.manicki.messagebox.MessageBox;
import javafx.application.Platform;

import java.awt.*;

public class HideApplicationPopupMenu {
    private MessageBox messageBox;
    private PopupMenu trayMenu;

    public HideApplicationPopupMenu() {
        this.messageBox = MessageBox.getInstance();
    }

    public PopupMenu createMenu() {
        trayMenu = new PopupMenu();
        addMenuItems();

        return trayMenu;
    }

    private void addMenuItems() {
        MenuItem restoreApplication = new MenuItem(messageBox.getLanguageFileProperty("restore"));
        restoreApplication.addActionListener(listener -> Platform.runLater(() ->
                new HideApplicationPopupMenuEvents().showMainWindow()));

        MenuItem closeApplication = new MenuItem(messageBox.getLanguageFileProperty("close"));
        closeApplication.addActionListener(listener -> Platform.runLater(() ->
                new HideApplicationPopupMenuEvents().closeMainWindow()));

        trayMenu.add(restoreApplication);
        trayMenu.addSeparator();
        trayMenu.add(closeApplication);
    }
}
