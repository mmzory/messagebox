package com.manicki.view.windows.mainwindow.components;

import com.manicki.controller.events.windows.mainwindow.MessagesTableEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.Messages;
import com.manicki.view.decoration.WindowComponentsDecoration;
import com.manicki.view.windows.mainwindow.components.messagestable.MessagesTableColumns;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.Callback;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MessagesTable extends TableView {
    private MessageBox messageBox;

    private MessagesTableEvents messageTableEvents;
    private String[] tabColumnLabels;
    private TableColumn sortColumn = null;
    private TableColumn[] tableColumn = new TableColumn[9];
    private int selectedRow;

    public MessagesTable() {
        super();
        this.messageBox = MessageBox.getInstance();
    }

    public void create() {
        this.setStyle(new WindowComponentsDecoration().setSoftTableViewDecoration());
        messageTableEvents = new MessagesTableEvents();
        messageTableEvents.setTableMouseAction();

        setColumnLabels();

        try {
            for (int column = 0; column < tabColumnLabels.length; column++) {
                final int j = column;
                tableColumn[column] = new TableColumn(tabColumnLabels[column]);
                tableColumn[column].setCellValueFactory(new Callback<TableColumn.CellDataFeatures<ObservableList,
                        String>, ObservableValue<String>>() {
                    @Override
                    public ObservableValue<String> call(TableColumn.CellDataFeatures<ObservableList, String> param) {
                        return new SimpleStringProperty(param.getValue().get(j).toString());
                    }
                });

                columnsConfiguration(column);

                this.getColumns().addAll(tableColumn[column]);
                this.setPrefHeight(Integer.MAX_VALUE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error on Building Table");
        }
    }

    private void setColumnLabels() {
        tabColumnLabels = new String[]{
                messageBox.getLanguageFileProperty("id"),
                messageBox.getLanguageFileProperty("date"),
                messageBox.getLanguageFileProperty("sender"),
                messageBox.getLanguageFileProperty("topic"),
                messageBox.getLanguageFileProperty("message"),
                messageBox.getLanguageFileProperty("computerName"),
                messageBox.getLanguageFileProperty("recipient"),
                messageBox.getLanguageFileProperty("new"),
                messageBox.getLanguageFileProperty("deleted")};
    }

    private void columnsConfiguration(int column) {
        if (column == MessagesTableColumns.DATE.ordinal()) {
            tableColumn[column].setPrefWidth(messageBox.getStage().getWidth() * 0.18);
            tableColumn[column].setSortType(TableColumn.SortType.DESCENDING);
            sortColumn = tableColumn[column];
        }
        if (column == MessagesTableColumns.SENDER.ordinal()) {
            tableColumn[column].setPrefWidth(messageBox.getStage().getWidth() * 0.12);
        }
        if (column == MessagesTableColumns.TOPIC.ordinal()) {
            tableColumn[column].setPrefWidth(messageBox.getStage().getWidth() * 0.6);
        }
        if (column == MessagesTableColumns.READED.ordinal()) {
            tableColumn[column].setPrefWidth(messageBox.getStage().getWidth() * 0.06);
        }
        if ((column == MessagesTableColumns.DATE.ordinal()) ||
                (column == MessagesTableColumns.SENDER.ordinal()) ||
                (column == MessagesTableColumns.READED.ordinal())) {
            tableColumn[column].setStyle("-fx-alignment: CENTER");
        }
        if ((column == MessagesTableColumns.ID.ordinal()) ||
                (column == MessagesTableColumns.MESSAGE.ordinal()) ||
                (column == MessagesTableColumns.COMPUTER_NAME.ordinal()) ||
                (column == MessagesTableColumns.RECIPIENT.ordinal()) ||
                (column == MessagesTableColumns.DELETED.ordinal())) {
            tableColumn[column].setVisible(false);
        }
    }

    public void fillContent(ResultSet resultSet) {
        byte dbColumnOffset = 1;
        ObservableList<ObservableList> data = FXCollections.observableArrayList();

        try {
            while (resultSet.next()) {
                ObservableList<String> row = FXCollections.observableArrayList();
                for (int dbColumn = 1; dbColumn <= resultSet.getMetaData().getColumnCount(); dbColumn++) {
                    if (dbColumn == (MessagesTableColumns.DATE.ordinal() + dbColumnOffset)) {
                        row.add(new Messages().getDate(resultSet));
                    } else if (dbColumn == (MessagesTableColumns.RECIPIENT.ordinal() + dbColumnOffset)) {
                        if (messageBox.getMainWindowView().getChoiceMessageBoxPane()
                                .getBoxType().equals(messageBox.getLanguageFileProperty("outbox"))) {
                            row.set(MessagesTableColumns.SENDER.ordinal(), resultSet.getString(dbColumn));
                        }
                        row.add(resultSet.getString(dbColumn));
                    } else if (dbColumn == (MessagesTableColumns.READED.ordinal() + dbColumnOffset)) {
                        if (!resultSet.getString(dbColumn).equals("0")) {
                            row.add(" ");
                        } else {
                            row.add("X");
                        }
                    } else {
                        row.add(resultSet.getString(dbColumn));
                    }
                }
                data.add(row);
            }

            this.setItems(data);
            this.getSortOrder().add(sortColumn);

        } catch (SQLException e) {
            System.out.println("Error on Building Data");
        }
    }

    public void setSelectedRow(int row) {
        this.selectedRow = row;
    }

    public int getSelectedRow() {
        return selectedRow;
    }

    public int getSelectedRowIndex() {
        return this.getSelectionModel().getSelectedIndex();
    }

    public void setTableColumnName(int column, String name) {
        tableColumn[column].setText(name);
    }

//        tvDatabaseRecords.setContextMenu(contextMenu);

}
