package com.manicki.view.windows;

import com.manicki.controller.events.windows.DefaultWindowsEvents;
import com.manicki.controller.events.windows.SearchMessageWindowEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.Windows;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class SearchMessageWindow implements Windows {
    private static SearchMessageWindow instance = null;
    private MessageBox messageBox;

    private Stage stage;
    private Label lSearch, lBox, lSearchingIn;
    private TextField tfSearch;
    private ComboBox searchingIn, choiceBox;
    private CheckBox cbIncludeDeletedMessages;
    private SearchMessageWindowEvents searchMessageWindowEvents;

    private SearchMessageWindow() {
        this.messageBox = MessageBox.getInstance();
        messageBox.getMainWindowView().getOpenedWindows().add(this);
        this.searchMessageWindowEvents = new SearchMessageWindowEvents();
    }

    public static SearchMessageWindow getInstance() {
        if (instance == null) {
            instance = new SearchMessageWindow();
            instance.create();
        }
        instance.getStage().toFront();
        return instance;
    }

    private void destruct() {
        instance = null;
    }

    private void create() {
        lSearch = new Label(messageBox.getLanguageFileProperty("search") + ":");
        lBox = new Label(messageBox.getLanguageFileProperty("box") + ":");
        lSearchingIn = new Label(messageBox.getLanguageFileProperty("search") + " "
                + messageBox.getLanguageFileProperty("in") + ":");
        tfSearch = new TextField();
        choiceBox = new ComboBox();
        choiceBox.getItems().addAll(messageBox.getLanguageFileProperty("inbox"),
                messageBox.getLanguageFileProperty("outbox"),
                messageBox.getLanguageFileProperty("in_outbox")
        );
        choiceBox.setValue(choiceBox.getItems().get(0));

        searchingIn = new ComboBox();
        searchingIn.getItems().addAll(messageBox.getLanguageFileProperty("topic3"),
                messageBox.getLanguageFileProperty("content"),
                messageBox.getLanguageFileProperty("topic3") + " " +
                        messageBox.getLanguageFileProperty("and") + " " +
                        messageBox.getLanguageFileProperty("content")
        );
        searchingIn.setValue(searchingIn.getItems().get(1));
        searchingIn.setMaxWidth(Integer.MAX_VALUE);
        cbIncludeDeletedMessages = new CheckBox(messageBox.getLanguageFileProperty("includeDeletedMessages"));

        GridPane searchBox = createSearchForm();

        Button bSearch = new Button(messageBox.getLanguageFileProperty("search"));
        bSearch.setOnAction(event -> searchMessageWindowEvents
                .event("search", choiceBox.getValue().toString(), searchingIn.getValue().toString(), tfSearch.getText(), cbIncludeDeletedMessages.isSelected()));
        Button bCancel = new Button(messageBox.getLanguageFileProperty("cancel"));
        bCancel.setOnAction(event -> close());
        HBox buttonsBox = new HBox(Windows.SPACING * 2, bSearch, bCancel);
        buttonsBox.setAlignment(Pos.CENTER);

        VBox mainPane = new VBox(Windows.SPACING * 2, searchBox, buttonsBox);
        mainPane.setPadding(new Insets(Windows.SPACING));
        mainPane.setAlignment(Pos.CENTER);
        Scene scene = new Scene(mainPane);
        this.stage = new Stage();
        this.stage.setScene(scene);
        setStageTitle(this.stage, messageBox.getLanguageFileProperty("search") + " "
                + messageBox.getLanguageFileProperty("messages").toLowerCase());
        this.stage.setOnCloseRequest(event -> close());
        setWindowIcon(this.stage);
        new DefaultWindowsEvents().setWindowLocationInCenterOfMainWindowAndShow(this.stage);

        this.searchMessageWindowEvents.setShortcutKeys(scene, bSearch, bCancel);
    }

    public Stage getStage() {
        return this.stage;
    }

    @Override
    public void close() {
        messageBox.getMainWindowView().getOpenedWindows().remove(this);
        this.stage.close();
        destruct();
    }

    private GridPane createSearchForm() {
        int row = 0;
        GridPane searchForm = new GridPane();
        searchForm.setAlignment(Pos.TOP_CENTER);
        searchForm.setVgap(Windows.SPACING);
        searchForm.setHgap(Windows.SPACING);
        searchForm.setPadding(new Insets(Windows.SPACING));
        searchForm.addRow(row, lSearch, tfSearch);
        searchForm.addRow(++row, lSearchingIn, searchingIn);
        searchForm.addRow(++row, lBox, choiceBox);
        searchForm.addRow(++row, cbIncludeDeletedMessages);

        return searchForm;
    }
}
