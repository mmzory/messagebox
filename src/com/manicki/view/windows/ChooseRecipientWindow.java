package com.manicki.view.windows;

import com.manicki.controller.events.windows.ChooseRecipientWindowEvents;
import com.manicki.controller.events.windows.DefaultWindowsEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.Windows;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ChooseRecipientWindow implements Windows {
    private final double STAGE_MIN_WIDTH = 200;

    private MessageBox messageBox;
    private CreateMessageWindow createMessageWindow;
    private Stage stage;

    private Set<String> databaseUsers;
    private List<CheckBox> recipients;

    public ChooseRecipientWindow(CreateMessageWindow createMessageWindow) {
        this.messageBox = MessageBox.getInstance();
        this.createMessageWindow = createMessageWindow;
        init();
    }

    private void init() {
        messageBox.getMainWindowView().getOpenedWindows().add(this);
        databaseUsers = new ChooseRecipientWindowEvents().getDatabaseUsers();
        recipients = new ArrayList<>();
    }

    public void create() {
        VBox mainPane = new VBox();
        checkChoosedRecipientsAndSelectChoiceBoxes(mainPane);

        Button bAccept = new Button(messageBox.getLanguageFileProperty("accept"));
        bAccept.setOnAction(event -> new ChooseRecipientWindowEvents()
                .putRecipientsToCreateMessageWindowField(this, createMessageWindow, recipients));
        Button bCancel = new Button(messageBox.getLanguageFileProperty("cancel"));
        bCancel.setOnAction(event -> close());

        HBox buttonsBox = new HBox(Windows.SPACING, bAccept, bCancel);
        buttonsBox.setPadding(new Insets(Windows.SPACING));
        buttonsBox.setAlignment(Pos.CENTER);

        mainPane.getChildren().add(buttonsBox);
        mainPane.setPadding(new Insets(Windows.SPACING));
        mainPane.setSpacing(Windows.SPACING);

        Scene scene = new Scene(mainPane);
        this.stage = new Stage();
        this.stage.setScene(scene);
        setStageTitle(this.stage, messageBox.getLanguageFileProperty("addUser") + " "
                + messageBox.getLanguageFileProperty("recipients"));
        this.stage.setMinWidth(STAGE_MIN_WIDTH);
        this.stage.setOnCloseRequest(event -> close());
        setWindowIcon(this.stage);
        new DefaultWindowsEvents().setWindowLocationInCenterOfMainWindowAndShow(this.stage);
        this.stage.setMinHeight(this.stage.getHeight());
    }

    public void toFront() {
        this.stage.toFront();
    }

    private void checkChoosedRecipientsAndSelectChoiceBoxes(VBox mainPane) {
        List<String> tfRecipientsList = new ArrayList<>();
        for (String recipientName : createMessageWindow.getTfRecipients()
                .replaceAll(" ", "").split(";")) {
            tfRecipientsList.add(recipientName);
        }

        for (String user : databaseUsers) {
            CheckBox userBox = new CheckBox(user);
            recipients.add(userBox);
            if (tfRecipientsList.contains(user)) {
                userBox.setSelected(true);
            }
            mainPane.getChildren().add(userBox);
        }
    }

    @Override
    public Stage getStage() {
        return this.stage;
    }

    @Override
    public void close() {
        createMessageWindow.setChooseRecipientWindowExists(false);
        messageBox.getMainWindowView().getOpenedWindows().remove(this);
        this.stage.close();
    }
}
