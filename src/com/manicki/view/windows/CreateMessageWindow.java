package com.manicki.view.windows;

import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.controller.events.windows.CreateMessageWindowEvents;
import com.manicki.controller.events.windows.DefaultWindowsEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.Windows;
import com.manicki.view.decoration.WindowComponentsDecoration;
import com.manicki.view.windows.mainwindow.components.messagestable.MessagesTableColumns;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.sql.SQLException;

public class CreateMessageWindow implements Windows {
    private final byte MAX_TOPIC_LENGTH = 80;

    private MessageBox messageBox;
    private CreateMessageWindowEvents createMessageWindowEvents;
    private ChooseRecipientWindow chooseRecipientWindow;
    private Stage stage;
    private TextArea taMessage;
    private TextField tfRecipient;
    private TextField tfTopic;
    private Label lTopicLength;
    private boolean chooseRecipientWindowExists = false;

    private int topicLength;

    public CreateMessageWindow() {
        this.messageBox = MessageBox.getInstance();
        this.messageBox.getMainWindowView().getOpenedWindows().add(this);
        this.createMessageWindowEvents = new CreateMessageWindowEvents();
    }

    public Scene create(Stage stage, String messageType, String[] messageContent) {
        Label recipient = new Label(messageBox.getLanguageFileProperty("recipient") + ": ");
        recipient.setMinWidth(Windows.MIN_WIDTH_SIZE);
        Label topic = new Label(messageBox.getLanguageFileProperty("topic") + ": ");
        Label message = new Label(messageBox.getLanguageFileProperty("message") + ": ");
        lTopicLength = new Label();

        tfRecipient = new TextField();
        tfRecipient.setPrefWidth(Integer.MAX_VALUE);
        tfRecipient.setStyle(new WindowComponentsDecoration().setSoftTableViewDecoration());
        tfRecipient.setEditable(false);
        tfTopic = new TextField();
        tfTopic.setStyle(new WindowComponentsDecoration().setSoftTableViewDecoration());
        tfTopic.setOnKeyReleased(keyReleased -> lTopicLength.setText(checkTopicLength()));

        Button bAddUsers = new Button(messageBox.getLanguageFileProperty("addUser"));
        bAddUsers.setMinWidth(Windows.MIN_WIDTH_SIZE);
        bAddUsers.setOnAction(event -> createMessageWindowEvents
                .createOrShowChooseRecipientWindow(this, chooseRecipientWindow));


        byte row = 0;
        GridPane messageDetails = new GridPane();
        messageDetails.setVgap(Windows.SPACING / 2);
        messageDetails.setHgap(Windows.SPACING);
        messageDetails.addRow(row++, recipient, tfRecipient, bAddUsers);
        messageDetails.addRow(row++, topic, tfTopic);
        messageDetails.add(lTopicLength, 1, row++);

        Separator separator = new Separator();

        taMessage = new TextArea();
        taMessage.setWrapText(true);
        taMessage.setPrefHeight(Integer.MAX_VALUE);
        taMessage.setStyle(new WindowComponentsDecoration().setSoftTableViewDecoration());

        Button bSend = new Button(messageBox.getLanguageFileProperty("send"));
        bSend.setMinWidth(Windows.MIN_WIDTH_SIZE);
        bSend.setId("send");
        bSend.setOnAction(event -> checkIfAnyFieldIsNotEmptyBeforeSendMessage());
        Button bCancel = new Button(messageBox.getLanguageFileProperty("cancel"));
        bCancel.setMinWidth(Windows.MIN_WIDTH_SIZE);
        bCancel.setOnAction(event -> confirmToCloseWindow(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST)));

        HBox buttonsPane = new HBox(Windows.SPACING, bSend, bCancel);
        buttonsPane.setAlignment(Pos.CENTER);

        VBox mainPane = new VBox(Windows.SPACING, messageDetails, separator, message, taMessage, buttonsPane);
        mainPane.setPadding(new Insets(Windows.SPACING));

        Scene scene = new Scene(mainPane);

        if (stage != null) {
            this.stage = stage;
        } else {
            this.stage = new Stage();
            this.stage.setScene(scene);
            setStageTitle(this.stage, messageBox.getLanguageFileProperty("createMessageWindowTitle"));
            new DefaultWindowsEvents().setWindowLocationAndSize(this.stage);
            setWindowIcon(this.stage);
            this.stage.show();
        }

        this.stage.setOnCloseRequest(closeEvent -> confirmToCloseWindow(closeEvent));

        taMessage.requestFocus();

        if (messageType.equals("forward")) {
            setForwardMessageView(messageContent);
        } else if (messageType.equals("answer")) {
            setAnswerMessageView(messageContent);
        }

        lTopicLength.setText(checkTopicLength());
        createMessageWindowEvents.setShortcutKeys(this, scene, bSend);

        return scene;
    }

    private void setForwardMessageView(String[] messageContent) {
        stage.setTitle(messageBox.getLanguageFileProperty("version") + "- "
                + messageBox.getLanguageFileProperty("forward") + " "
                + messageBox.getLanguageFileProperty("message").toLowerCase());
        tfTopic.setText("FW: " + messageContent[MessagesTableColumns.TOPIC.ordinal()]);
        taMessage.setText("\n\n_______________________________________\n"
                + messageBox.getLanguageFileProperty("msgForwarded")
                .replace("<user>", messageContent[MessagesTableColumns.SENDER.ordinal()])
                .replace("<date>", messageContent[MessagesTableColumns.DATE.ordinal()]
                        .replace(" -", ", godz.")) + "\n"
                + messageContent[MessagesTableColumns.MESSAGE.ordinal()]);
    }

    private void setAnswerMessageView(String[] messageContent) {
        stage.setTitle(messageBox.getLanguageFileProperty("version") + "- "
                + messageBox.getLanguageFileProperty("answer"));
        tfRecipient.setText(messageContent[MessagesTableColumns.SENDER.ordinal()]);
        tfTopic.setText("RE: " + messageContent[MessagesTableColumns.TOPIC.ordinal()]);
        taMessage.setText("\n\n_______________________________________\n"
                + messageBox.getLanguageFileProperty("msgAnswered")
                .replace("<user>", messageContent[MessagesTableColumns.SENDER.ordinal()])
                .replace("<date>", messageContent[MessagesTableColumns.DATE.ordinal()]
                        .replace(" -", ", godz.")) + "\n"
                + messageContent[MessagesTableColumns.MESSAGE.ordinal()]);
    }

    private String checkTopicLength() {
        topicLength = tfTopic.getLength();

        lTopicLength.setStyle(new WindowComponentsDecoration().setFontSize(10));
        if (topicLength > MAX_TOPIC_LENGTH) {
            lTopicLength.setStyle(new WindowComponentsDecoration().setTextColorOnRed());
        } else {
            lTopicLength.setStyle(new WindowComponentsDecoration().setTextColorOnBlack());
        }
        return messageBox.getLanguageFileProperty("signCounter") + topicLength + "/" + MAX_TOPIC_LENGTH;
    }

    private void checkIfAnyFieldIsNotEmptyBeforeSendMessage() {
        try {
            String errorMessage = createMessageWindowEvents
                    .checkIfFieldsAreNotEmpty(tfRecipient, tfTopic, taMessage);
            if (errorMessage == null) {
                createMessageWindowEvents.sendMessage(this);
            } else {
                Alert alert = new CreateAlert().recipientTopicOrMessageFieldIsEmpty(errorMessage);
                alert.show();
            }
        } catch (SQLException e) {
            Alert alert = new CreateAlert().connectionErrorOrIncorrectData();
            alert.show();
            e.printStackTrace();
        }
    }

    public void setChooseRecipientWindow(ChooseRecipientWindow chooseRecipientWindow) {
        this.chooseRecipientWindow = chooseRecipientWindow;
    }

    public void setChooseRecipientWindowExists(boolean exists) {
        this.chooseRecipientWindowExists = exists;
    }

    public boolean isChooseRecipientWindowExists() {
        return chooseRecipientWindowExists;
    }

    public void setTfRecipientText(String recipients) {
        tfRecipient.setText(recipients);
    }

    public String getTfRecipients() {
        return tfRecipient.getText();
    }

    public String getMessage() {
        return taMessage.getText();
    }

    public String getTopic() {
        return tfTopic.getText();
    }

    public void toFront() {
        stage.toFront();
    }

    public void confirmToCloseWindow(WindowEvent closeEvent) {
        if (!taMessage.getText().isEmpty()) {
            createMessageWindowEvents.confirmToCloseWindow(this, closeEvent);
        } else {
            close();
        }
    }

    @Override
    public void close() {
        if (messageBox.getMainWindowView().getOpenedWindows().contains(chooseRecipientWindow)) {
            chooseRecipientWindow.close();
        }
        messageBox.getMainWindowView().getOpenedWindows().remove(this);
        stage.close();
    }

    public Stage getStage() {
        return this.stage;
    }
}
