package com.manicki.view.windows;

import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.controller.events.windows.DefaultWindowsEvents;
import com.manicki.controller.events.windows.ShowMessageWindowEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.Messages;
import com.manicki.view.Windows;
import com.manicki.view.decoration.WindowComponentsDecoration;
import com.manicki.view.windows.mainwindow.components.messagestable.MessagesTableColumns;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.sql.SQLException;

public class ShowMessageWindow implements Windows {
    private MessageBox messageBox;
    private ShowMessageWindowEvents showMessageWindowEvents;
    private Stage stage;
    private Scene scene;

    private String[] messageContent;

    public ShowMessageWindow() {
        this.messageBox = MessageBox.getInstance();
        this.messageBox.getMainWindowView().getOpenedWindows().add(this);
        getMessageContent();
        this.showMessageWindowEvents = new ShowMessageWindowEvents();
    }

    public void create() {
        Label sender = new Label(this.messageBox.getLanguageFileProperty("sender") + ": ");
        sender.setMinWidth(100);
        Label date = new Label(this.messageBox.getLanguageFileProperty("date") + ": ");
        Label topic = new Label(this.messageBox.getLanguageFileProperty("topic") + ": ");
        Label message = new Label(this.messageBox.getLanguageFileProperty("message") + ": ");

        Label messageDate = new Label(messageContent[MessagesTableColumns.DATE.ordinal()]
                .replace(" -", ", " + this.messageBox.getLanguageFileProperty("hour")));
        TextField tfSender = new TextField(messageContent[MessagesTableColumns.SENDER.ordinal()]);
        tfSender.setEditable(false);
        tfSender.setPrefWidth(Integer.MAX_VALUE);
        tfSender.setStyle(new WindowComponentsDecoration().setSoftTableViewDecoration());
        TextField tfTopic = new TextField(messageContent[MessagesTableColumns.TOPIC.ordinal()]);
        tfTopic.setEditable(false);
        tfTopic.setStyle(new WindowComponentsDecoration().setSoftTableViewDecoration());

        HBox dateBox = new HBox(Windows.SPACING, date, messageDate);
        dateBox.setAlignment(Pos.CENTER_RIGHT);

        byte row = 0;
        GridPane messageDetails = new GridPane();
        messageDetails.setVgap(Windows.SPACING / 2);
        messageDetails.setHgap(Windows.SPACING);
        messageDetails.addRow(row++, sender, tfSender);
        messageDetails.addRow(row++, topic, tfTopic);

        Separator separator = new Separator();

        TextArea taMessage = new TextArea(messageContent[MessagesTableColumns.MESSAGE.ordinal()]);
        taMessage.setEditable(false);
        taMessage.setWrapText(true);
        taMessage.setPrefHeight(Integer.MAX_VALUE);
        taMessage.setStyle(new WindowComponentsDecoration().setSoftTableViewDecoration());

        Button bForward = new Button(this.messageBox.getLanguageFileProperty("forward"));
        bForward.setMinWidth(Windows.MIN_WIDTH_SIZE);
        bForward.setId("forward");
        bForward.setOnAction(event -> forwardOrAnswerEvent(bForward));
        Button bAnswer = new Button(this.messageBox.getLanguageFileProperty("answer"));
        bAnswer.setMinWidth(Windows.MIN_WIDTH_SIZE);
        bAnswer.setId("answer");
        bAnswer.setOnAction(event -> forwardOrAnswerEvent(bAnswer));
        Button bCancel = new Button(this.messageBox.getLanguageFileProperty("cancel"));
        bCancel.setMinWidth(Windows.MIN_WIDTH_SIZE);
        bCancel.setOnAction(event -> close());

        HBox buttonsPane = new HBox(Windows.SPACING, bForward, bAnswer, bCancel);
        buttonsPane.setAlignment(Pos.CENTER);

        VBox mainPane = new VBox(Windows.SPACING, dateBox, messageDetails, separator, message, taMessage, buttonsPane);
        mainPane.setPadding(new Insets(Windows.SPACING));

        this.scene = new Scene(mainPane);
        this.stage = new Stage();
        this.stage.setScene(this.scene);
        setStageTitle(this.stage, this.messageBox.getLanguageFileProperty("message")
                + ": "
                + messageContent[MessagesTableColumns.TOPIC.ordinal()]);
        new DefaultWindowsEvents().setWindowLocationAndSize(this.stage);
        this.stage.setOnCloseRequest(event -> close());
        setWindowIcon(this.stage);
        this.stage.show();

        this.showMessageWindowEvents.setShortcutKeys(this, this.scene);
        taMessage.requestFocus();
    }

    private void forwardOrAnswerEvent(Button eventButton) {
        this.scene = this.showMessageWindowEvents.event(this.stage, eventButton, this.messageContent);
        this.stage.setScene(this.scene);
        removeFromWindowsList();
    }

    private void getMessageContent() {
        try {
            long id = new Messages().getIdOfSelectedMessage();
            this.messageContent = new Messages().getMessageDetails(id);
        } catch (SQLException e) {
            Alert sqlAlert = new CreateAlert().connectionErrorOrIncorrectData();
            sqlAlert.show();
        }
    }

    private void removeFromWindowsList() {
        this.messageBox.getMainWindowView().getOpenedWindows().remove(this);
    }

    @Override
    public Stage getStage() {
        return this.stage;
    }

    @Override
    public void close() {
        removeFromWindowsList();
        this.stage.close();
    }
}