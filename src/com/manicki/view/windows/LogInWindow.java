package com.manicki.view.windows;

import com.manicki.controller.errorsandexceptions.CreateAlert;
import com.manicki.controller.events.windows.DefaultWindowsEvents;
import com.manicki.controller.events.windows.LogInWindowEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.Windows;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class LogInWindow implements Windows {
    private static LogInWindow instance;
    private MessageBox messageBox;
    private LogInWindowEvents logInWindowEvents;

    private Label username, password;
    private TextField usernameTextField;
    private PasswordField passwordField;
    private Stage stage;

    private LogInWindow() {
        this.messageBox = MessageBox.getInstance();
        this.logInWindowEvents = new LogInWindowEvents();
        this.messageBox.getMainWindowView().getOpenedWindowsNoCloseConfirmation().add(this);
    }

    public static LogInWindow createOrGetInstance() {
        if (instance == null) {
            instance = new LogInWindow();
            instance.createStage();
        }
        return instance;
    }

    private void destruct() {
        if (instance != null) {
            instance = null;
        }
    }

    private void createStage() {
        this.stage = new Stage();
    }

    public void create() {
        username = new Label(messageBox.getLanguageFileProperty("user") + ":");
        usernameTextField = new TextField(messageBox.getConfiguration().getConfigFile().getProperty("user"));

        password = new Label(messageBox.getLanguageFileProperty("password") + ":");
        passwordField = new PasswordField();

        GridPane loginForm = createLogInForm();

        Button bConnect = new Button(messageBox.getLanguageFileProperty("connect"));
        bConnect.setOnAction(event -> {
            logInWindowEvents = new LogInWindowEvents();
            if (!usernameTextField.getText().isEmpty()) {
                if (!passwordField.getText().isEmpty()) {
                    boolean connectionError = logInWindowEvents.connectToDatabase(usernameTextField.getText(),
                            passwordField.getText());
                    if (!connectionError) {
                        close();
                    }
                } else {
                    Alert alert = new CreateAlert().passwordFieldIsEmpty();
                    alert.setOnCloseRequest(event2 -> passwordField.requestFocus());
                    alert.show();
                }
            } else {
                Alert alert = new CreateAlert().usernameFieldIsEmpty();
                alert.setOnCloseRequest(event2 -> usernameTextField.requestFocus());
                alert.show();
            }
        });

        Button bCancel = new Button(messageBox.getLanguageFileProperty("cancel"));
        bCancel.setOnAction(event -> close());

        HBox buttonsBox = new HBox(Windows.SPACING, bConnect, bCancel);
        buttonsBox.setAlignment(Pos.CENTER_RIGHT);

        VBox mainPane = new VBox();
        mainPane.setAlignment(Pos.CENTER);
        mainPane.setPadding(new Insets(Windows.SPACING));
        mainPane.setSpacing(Windows.SPACING);
        mainPane.getChildren().addAll(loginForm, buttonsBox);

        Scene scene = new Scene(mainPane);

        this.stage.setResizable(false);
        this.stage.setScene(scene);
        setStageTitle(this.stage, messageBox.getLanguageFileProperty("logInWindowTitle"));
        this.stage.setOnCloseRequest(event -> close());
        setWindowIcon(this.stage);
        new DefaultWindowsEvents().setWindowLocationInCenterOfMainWindowAndShow(this.stage);

        logInWindowEvents.setShortcutKeys(scene, bConnect, bCancel);

        passwordField.requestFocus();
    }

    private GridPane createLogInForm() {
        int row = 0;
        GridPane loginForm = new GridPane();
        loginForm.setAlignment(Pos.TOP_CENTER);
        loginForm.setVgap(Windows.SPACING);
        loginForm.setHgap(Windows.SPACING);
        loginForm.setPadding(new Insets(Windows.SPACING));
        loginForm.addRow(row, username, usernameTextField);
        loginForm.addRow(++row, password, passwordField);

        return loginForm;
    }

    public PasswordField getPasswordField() {
        return passwordField;
    }

    @Override
    public Stage getStage() {
        return this.stage;
    }

    @Override
    public void close() {
        messageBox.getMainWindowView().getOpenedWindowsNoCloseConfirmation().remove(this);
        this.stage.close();
        this.destruct();
    }
}
