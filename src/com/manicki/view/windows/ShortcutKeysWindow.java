package com.manicki.view.windows;

import com.manicki.controller.events.windows.DefaultWindowsEvents;
import com.manicki.controller.events.windows.ShortcutKeysWindowsEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.Windows;
import com.manicki.view.decoration.WindowComponentsDecoration;
import com.manicki.view.windows.shortcutwindow.tabs.LogInWindowTab;
import com.manicki.view.windows.shortcutwindow.tabs.MainWindowTab;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TabPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ShortcutKeysWindow implements Windows {
    private static ShortcutKeysWindow instance = null;
    private ShortcutKeysWindowsEvents shortcutKeysWindowsEvents;
    private MessageBox messageBox;
    private Stage stage;

    private ShortcutKeysWindow() {
        this.messageBox = MessageBox.getInstance();
        this.messageBox.getMainWindowView().getOpenedWindowsNoCloseConfirmation().add(this);
        this.shortcutKeysWindowsEvents = new ShortcutKeysWindowsEvents();
    }

    public static ShortcutKeysWindow getInstance() {
        if (instance == null) {
            instance = new ShortcutKeysWindow();
            instance.create();
        }
        instance.stage.toFront();
        return instance;
    }

    private void destruct() {
        if (instance != null) {
            instance = null;
        }
    }

    public void create() {
        Label shortcutKeysTopic = new Label(this.messageBox.getLanguageFileProperty("shortcutKeys"));
        shortcutKeysTopic.setStyle(new WindowComponentsDecoration().setFontSize(15));
        VBox topicLabel = new VBox(Windows.SPACING, shortcutKeysTopic);
        topicLabel.setAlignment(Pos.CENTER);

        TabPane tabPane = new TabPane(
                new MainWindowTab().fillTabContent(),
                new LogInWindowTab().fillTabContent()
        );
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);
        tabPane.setMinSize(400, 300);
        tabPane.setPadding(new Insets(Windows.SPACING));

        Button closeButton = new Button(this.messageBox.getLanguageFileProperty("close"));
        closeButton.setMinWidth(Windows.MIN_WIDTH_SIZE);
        closeButton.setOnAction(event -> close());
        HBox buttonsPane = new HBox(closeButton);
        buttonsPane.setSpacing(Windows.SPACING);
        buttonsPane.setAlignment(Pos.CENTER);

        VBox mainPane = new VBox(Windows.SPACING, topicLabel, tabPane, buttonsPane);
        mainPane.setPadding(new Insets(Windows.SPACING));

        Scene scene = new Scene(mainPane);
        this.stage = new Stage();
        this.stage.setScene(scene);
        setStageTitle(this.stage, this.messageBox.getLanguageFileProperty("shortcutKeys"));
        this.stage.setOnCloseRequest(event -> close());
        setWindowIcon(this.stage);
        new DefaultWindowsEvents().setWindowLocationInCenterOfMainWindowAndShow(this.stage);

        this.shortcutKeysWindowsEvents.setShortcutKeys(scene, closeButton);
    }

    @Override
    public Stage getStage() {
        return this.stage;
    }

    @Override
    public void close() {
        this.messageBox.getMainWindowView().getOpenedWindowsNoCloseConfirmation().remove(this);
        this.stage.close();
        destruct();
    }
}
