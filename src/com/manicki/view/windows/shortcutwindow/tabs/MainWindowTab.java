package com.manicki.view.windows.shortcutwindow.tabs;

import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

public class MainWindowTab extends ShortcutsWindowsTab {
    public Tab fillTabContent() {
        return super.fillTabContent(getShortcutList(), messageBox.getLanguageFileProperty("mainWindow"));
    }

    private List<String> getShortcutList() {
        List<String> shortcutsList = new ArrayList<String>();

        shortcutKeys.createNewMessageShortcut(new Scene(new VBox()));
        shortcutsList.add(shortcutKeys.getShortcutKeyCombination() + ";" + shortcutKeys.getDescription());
        shortcutKeys.changeBoxShortcut(new Scene(new VBox()));
        shortcutsList.add(shortcutKeys.getShortcutKeyCombination() + ";" + shortcutKeys.getDescription());
        shortcutKeys.deleteMessageShortcut(new Scene(new VBox()));
        shortcutsList.add(shortcutKeys.getShortcutKeyCombination() + ";" + shortcutKeys.getDescription());
        shortcutKeys.openFindMessageWindow(new Scene(new VBox()));
        shortcutsList.add(shortcutKeys.getShortcutKeyCombination() + ";" + shortcutKeys.getDescription());
        shortcutKeys.openMessageShortcut(new Scene(new VBox()));
        shortcutsList.add(shortcutKeys.getShortcutKeyCombination() + ";" + shortcutKeys.getDescription());
        shortcutKeys.quitApplicationShortcut(new Scene(new VBox()));
        shortcutsList.add(shortcutKeys.getShortcutKeyCombination() + ";" + shortcutKeys.getDescription());
        shortcutKeys.signInOrOutShortcut(new Scene(new VBox()));
        shortcutsList.add(shortcutKeys.getShortcutKeyCombination() + ";" + shortcutKeys.getDescription());
        return shortcutsList;
    }
}
