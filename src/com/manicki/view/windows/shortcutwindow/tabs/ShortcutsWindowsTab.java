package com.manicki.view.windows.shortcutwindow.tabs;

import com.manicki.controller.events.ShortcutKeys;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.Windows;
import com.manicki.view.decoration.WindowComponentsDecoration;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.List;

public abstract class ShortcutsWindowsTab {
    protected MessageBox messageBox;
    protected ShortcutKeys shortcutKeys;

    public ShortcutsWindowsTab() {
        this.messageBox = MessageBox.getInstance();
        this.shortcutKeys = ShortcutKeys.getInstance();
    }

    public Tab fillTabContent(List<String> shortcutsList, String tabTitle) {
        Tab shortcutsTab = new Tab(tabTitle);
        VBox labelKeyCombinationsBox = new VBox();
        VBox labelDescriptionsBox = new VBox();
        labelKeyCombinationsBox.setSpacing(Windows.SPACING / 2);
        labelKeyCombinationsBox.setMinWidth(Windows.MIN_WIDTH_SIZE / 2);
        labelDescriptionsBox.setSpacing(Windows.SPACING / 2);
        for (String shortcut : shortcutsList) {
            String[] shortcutCombinationAndDescription = shortcut.split(";");
            labelKeyCombinationsBox.getChildren().add(new Label(shortcutCombinationAndDescription[0]));
            labelDescriptionsBox.getChildren().add(new Label("- " + shortcutCombinationAndDescription[1]));
        }
        HBox mainBox = new HBox(labelKeyCombinationsBox, labelDescriptionsBox);
        mainBox.setStyle(new WindowComponentsDecoration().getTabPaneDecoration());
        mainBox.setPadding(new Insets(Windows.SPACING));
        mainBox.setSpacing(Windows.SPACING);
        shortcutsTab.setContent(mainBox);
        return shortcutsTab;
    }
}
