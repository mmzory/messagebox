package com.manicki.view.windows.shortcutwindow.tabs;

import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

public class LogInWindowTab extends ShortcutsWindowsTab {
    public Tab fillTabContent() {
        Tab tab = super.fillTabContent(getShortcutList(), messageBox.getLanguageFileProperty("subWindows"));
        tab.setTooltip(new Tooltip(messageBox.getLanguageFileProperty("additionalWindows")));
        return tab;
    }

    private List<String> getShortcutList() {
        List<String> shortcutsList = new ArrayList<String>();
        shortcutKeys.pushAcceptButtonShortcut(new Scene(new VBox()), new Button());
        shortcutsList.add(shortcutKeys.getShortcutKeyCombination() + ";" + shortcutKeys.getDescription());
        shortcutKeys.pushCancelButtonShortcut(new Scene(new VBox()), new Button());
        shortcutsList.add(shortcutKeys.getShortcutKeyCombination() + ";" + shortcutKeys.getDescription());
        return shortcutsList;
    }
}
