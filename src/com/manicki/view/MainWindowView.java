package com.manicki.view;

import com.manicki.controller.events.windows.mainwindow.CloseApplication;
import com.manicki.controller.events.windows.mainwindow.MainWindowEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.view.windows.mainwindow.components.ChoiceMessageBoxPane;
import com.manicki.view.windows.mainwindow.components.MainWindowBottomPane;
import com.manicki.view.windows.mainwindow.components.MenuBarPane;
import com.manicki.view.windows.mainwindow.components.MessagesTable;
import javafx.geometry.Dimension2D;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class MainWindowView {
    private MessageBox messageBox;
    private Stage stage;
    private MainWindowEvents mainWindowEvents;
    private MenuBar menuBar;
    private MenuBarPane menuBarPane;
    private ChoiceMessageBoxPane choiceMessageBoxPane;
    private MessagesTable messagesTable;
    private MainWindowBottomPane mainWindowBottomPane;
    private Point2D windowLocation;
    private Dimension2D windowSize;
    private boolean isHidden = false;

    private List<Windows> openedWindows, openedWindowsNoCloseConfirmation;
    private BorderPane mainPane;

    public MainWindowView(MessageBox messageBox) {
        this.messageBox = MessageBox.getInstance();
        this.stage = messageBox.getStage();
        setMinStageSize();
        mainWindowEvents = new MainWindowEvents();
    }

    public void createViewContent() {
        openedWindows = new ArrayList<>();
        openedWindowsNoCloseConfirmation = new ArrayList<>();

        menuBarPane = new MenuBarPane();
        menuBar = menuBarPane.createContent();

        choiceMessageBoxPane = new ChoiceMessageBoxPane();
        mainWindowBottomPane = new MainWindowBottomPane();

        VBox topPane = new VBox(menuBar, choiceMessageBoxPane.createPane());
        messagesTable = new MessagesTable();
        VBox bottomPane = new VBox(mainWindowBottomPane.createPane());

        mainPane = new BorderPane();
        mainPane.setTop(topPane);
        mainPane.setCenter(messagesTable);
        mainPane.setBottom(bottomPane);

        Scene scene = new Scene(mainPane);
        stage.setScene(scene);
        stage.setTitle(messageBox.getLanguageFileProperty("version") + messageBox.getLanguageFileProperty("notLoggedIn"));
        mainWindowEvents.setWindowLocationAndSize();
        mainWindowEvents.setWindowIcon(this.stage);
        stage.show();

        mainWindowEvents.setMainWindowShortcutKeys(scene);
        stage.setOnCloseRequest(event -> new CloseApplication().showConfirmation(event));
    }

    public MainWindowEvents getMainWindowEvents() {
        return mainWindowEvents;
    }

    public MenuBar getMenuBar() {
        return menuBar;
    }

    public MenuBarPane getMenuBarPane() {
        return menuBarPane;
    }

    public BorderPane getMainPane() {
        return mainPane;
    }

    public MessagesTable getMessagesTable() {
        return messagesTable;
    }

    public ChoiceMessageBoxPane getChoiceMessageBoxPane() {
        return choiceMessageBoxPane;
    }

    public MainWindowBottomPane getMainWindowBottomPane() {
        return mainWindowBottomPane;
    }

    public List<Windows> getOpenedWindows() {
        return openedWindows;
    }

    public List<Windows> getOpenedWindowsNoCloseConfirmation() {
        return openedWindowsNoCloseConfirmation;
    }

    public List<Windows> getAllOpenedWindows() {
        List<Windows> allOpenedWindows = new ArrayList<>();
        allOpenedWindows.addAll(openedWindows);
        allOpenedWindows.addAll(openedWindowsNoCloseConfirmation);

        return allOpenedWindows;
    }

    public boolean isAnyWindowOpen() {
        return openedWindows.size() > 0;
    }

    public Point2D getWindowLocation() {
        return windowLocation;
    }

    public void setWindowLocation(Point2D windowLocation) {
        this.windowLocation = windowLocation;
    }

    public Dimension2D getWindowSize() {
        return windowSize;
    }

    public void setWindowSize(Dimension2D windowSize) {
        this.windowSize = windowSize;
    }

    private void setMinStageSize() {
        this.stage.setMinWidth(640);
        this.stage.setMinHeight(480);
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean hidden) {
        isHidden = hidden;
    }
}