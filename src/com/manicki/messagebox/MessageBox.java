package com.manicki.messagebox;

import com.manicki.controller.initialization.Configuration;
import com.manicki.service.database.Database;
import com.manicki.view.MainWindowView;
import javafx.application.Platform;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

import java.awt.*;
import java.util.Timer;

public class MessageBox {
    private static MessageBox instance = null;

    private Database database;
    private MainWindowView mainWindowView;
    private Configuration configuration;
    private Stage stage;
    private String username;
    private Timer checkNewMessagesThread;
    private SystemTray systemTray;

    private MessageBox() {}

    public static MessageBox getInstance() {
        if (instance == null) {
            instance = new MessageBox();
        }
        return instance;
    }

    public void init(Stage stage) {
        Platform.setImplicitExit(false);
        this.stage = stage;
        this.username = new String();
        configuration = new Configuration();
        configuration.loadSettingsAndLanguageFile();
        mainWindowView = new MainWindowView(this);
        mainWindowView.createViewContent();
    }

    public MainWindowView getMainWindowView() {
        return mainWindowView;
    }

    public Stage getStage() {
        return this.stage;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public String getLanguageFileProperty(String property) {
        return getConfiguration().getLanguageFile().getProperty(property);
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    public Database getDatabase() {
        return database;
    }

    public MenuItem getMenubarItem(int menusIndex, int itemIndex) {
        return getMainWindowView().getMenuBar().getMenus().get(menusIndex)
                .getItems().get(itemIndex);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Timer getCheckNewMessagesThread() {
        return checkNewMessagesThread;
    }

    public void setCheckNewMessagesThread(Timer checkNewMessagesThread) {
        this.checkNewMessagesThread = checkNewMessagesThread;
    }

    public SystemTray getSystemTray() {
        return systemTray;
    }

    public void setSystemTray(SystemTray systemTray) {
        this.systemTray = systemTray;
    }
}
