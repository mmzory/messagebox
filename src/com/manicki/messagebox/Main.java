package com.manicki.messagebox;

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        MessageBox messageBox = MessageBox.getInstance();
        messageBox.init(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
