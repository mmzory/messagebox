package com.manicki.service;

import com.manicki.controller.events.MessagesEvents;
import com.manicki.messagebox.MessageBox;
import com.manicki.service.database.SqlQuerys;
import com.manicki.view.windows.mainwindow.components.messagestable.MessagesTableColumns;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;

public class Messages {
    private MessageBox messageBox;
    private MessagesEvents messagesEvents;
    private byte dbOffset = 1;

    public Messages() {
        this.messageBox = MessageBox.getInstance();
        this.messagesEvents = new MessagesEvents();
    }

    public long getIdOfSelectedMessage() {
        String selectedRow = messageBox.getMainWindowView().getMessagesTable().getSelectionModel().getSelectedItem().toString();
        String[] selectedRowParts = selectedRow.substring(1, selectedRow.length() - 1).split(",");
        long selectedRowId = Long.parseLong(selectedRowParts[0]);

        return selectedRowId;
    }

    public String[] getMessageDetails(long messageId) throws SQLException {
        ResultSet resultSet = new SqlQuerys().readMessage(messageId);
        int columnCount = resultSet.getMetaData().getColumnCount();
        String[] row = new String[columnCount];

        while (resultSet.next()) {
            for (int column = 1; column <= columnCount; column++) {
                if (column == MessagesTableColumns.DATE.ordinal() + dbOffset) {
                    row[column - 1] = getDate(resultSet);
                } else {
                    row[column - 1] = resultSet.getString(column);
                }
            }
        }

        return row;
    }

    public String getDate(ResultSet resultSet) throws SQLException {
        String date = resultSet.getDate(MessagesTableColumns.DATE.ordinal() + dbOffset)
                .toLocalDate().format(DateTimeFormatter.ofPattern("yyyy.MM.dd")) + " - "
                + resultSet.getTime(MessagesTableColumns.DATE.ordinal() + dbOffset)
                .toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm"));
        return date;
    }

    public MessagesEvents getMessagesEvents() {
        return messagesEvents;
    }
}
