package com.manicki.service;

import com.manicki.service.database.SqlQuerys;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Set;
import java.util.TreeSet;

public class Users {

    public Set<String> getDatabaseUsers() throws SQLException {
        ResultSet resultSet = new SqlQuerys().getDbUsers();
        Set<String> dbUsers = new TreeSet<>();
        while (resultSet.next()) {
            dbUsers.add(resultSet.getString(1));
        }

        return dbUsers;
    }
}
