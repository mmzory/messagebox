package com.manicki.service.database;

import com.manicki.controller.errorsandexceptions.DatabaseConnectionException;
import com.manicki.controller.errorsandexceptions.SQLQueryException;
import com.manicki.messagebox.MessageBox;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Database {
    private MessageBox messageBox;
    private DatabaseConnection databaseConnection;
    private Connection connection;
    private Statement statement;

    public Database() {
        this.messageBox = MessageBox.getInstance();
    }

    public void connectToDatabase(String username, String password) throws DatabaseConnectionException {
        messageBox.setDatabase(this);
        databaseConnection = new DatabaseConnection();
        boolean connectionError = databaseConnection.connect(username, password);

        if (connectionError) {
            throw new DatabaseConnectionException();
        }
    }

    public void createDatabase() {
        try {
            this.statement = this.connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            this.statement.executeUpdate("CREATE DATABASE MessageBox");
        } catch (SQLException sqle) {
            System.err.println("SQL exception: " + sqle.getMessage());
        }
    }

    public void createTables() throws SQLQueryException {
        boolean creatingTableError = false;

        try {
            this.statement.executeUpdate("USE MessageBox");
            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS Message"
                    + "(IdMessage BIGINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, SendDate DATETIME(0), Sender VARCHAR(30), Topic VARCHAR(70), "
                    + "Message TEXT, CompName VARCHAR(30), Recipient VARCHAR(20), Readed TINYINT, Deleted TINYINT"
                    + ") CHARACTER SET = cp1250");

            this.statement.executeUpdate("CREATE TABLE IF NOT EXISTS MbUsers"
                    + "(IdUser TINYINT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, UserName VARCHAR(30) UNIQUE, Online TINYINT) CHARACTER SET = cp1250");
        } catch (SQLException ex) {
            creatingTableError = true;
        }

        if (creatingTableError) {
            throw new SQLQueryException();
        }
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void setStatement(Statement statement) {
        this.statement = statement;
    }

    public Statement getStatement() {
        return this.statement;
    }

    public DatabaseConnection getDatabaseConnection() {
        return databaseConnection;
    }
}
