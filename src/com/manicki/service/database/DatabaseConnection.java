package com.manicki.service.database;

import com.manicki.controller.Logger;
import com.manicki.controller.errorsandexceptions.DatabaseConnectionException;
import com.manicki.messagebox.MessageBox;

import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {
    private MessageBox messageBox;

    public DatabaseConnection() {
        this.messageBox = MessageBox.getInstance();
    }

    public boolean connect(String login, String password) throws DatabaseConnectionException {
        boolean connectionError = false;

        try {
            Class.forName("org.mariadb.jdbc.Driver");
            messageBox.getDatabase().setConnection(DriverManager.getConnection("jdbc:mariadb://" +
                            messageBox.getConfiguration().getConfigFile().getProperty("serverIp") + ":" +
                            messageBox.getConfiguration().getConfigFile().getProperty("serverPort"),
                    login,
                    password));
        } catch (ClassNotFoundException cnfe) {
            connectionError = true;
            Logger.getInstance().logError("ClassNotFound exception: " + cnfe.getMessage());
        } catch (SQLException e) {
            connectionError = true;
            Logger.getInstance().logError("SQL exception: " + e.getMessage());
        }

        return connectionError;
    }

//    public void checkConnection(MessageBox messageBox, String enteredPassword) {
//        Connection tmpConnection = this.connection;
//        connect(messageBox.getUsername(), enteredPassword);
//        this.connection = tmpConnection;
//    }

    public void disconnect() {
        boolean error = false;
        if (messageBox.getDatabase().getConnection() != null) {
            try {
                try {
                    if (!messageBox.getDatabase().getConnection().isClosed()) {
//                        this.connection.createStatement().executeQuery("UPDATE MbUsers SET Online = '0' WHERE UserName = '" + messageBox.getUsername() + "'");
                    }
                } catch (SQLException ex) {
                    Logger.getInstance().logError("Database connection broken...");
                }
                messageBox.getDatabase().getConnection().close();
                messageBox.getDatabase().getStatement().close();
            } catch (SQLException ex) {
                Logger.getInstance().logError("Database connection broken...");
//                Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (!error) {
            messageBox.getDatabase().setConnection(null);
            messageBox.getDatabase().setStatement(null);
        }
    }
}
