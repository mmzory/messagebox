package com.manicki.service.database;

import com.manicki.messagebox.MessageBox;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SqlQuerys {
    private MessageBox messageBox;

    public SqlQuerys() {
        this.messageBox = MessageBox.getInstance();
    }

    public ResultSet getInboxMessages() throws SQLException {
        return messageBox.getDatabase().getConnection().createStatement()
                .executeQuery("SELECT * from Message WHERE Recipient='" +
                        messageBox.getUsername() +
                        "' AND Deleted='0'");
    }

    public ResultSet getOutboxMessages() throws SQLException {
        return messageBox.getDatabase().getConnection().createStatement()
                .executeQuery("SELECT * from Message WHERE Sender='" +
                        messageBox.getUsername() + "'");
    }

    public ResultSet readMessage(long index) throws SQLException {
        return messageBox.getDatabase().getConnection().createStatement()
                .executeQuery("SELECT * from Message WHERE IdMessage='" + index + "'");
    }

    public void sendMessage(String recipient, String topic, String message) throws SQLException {
        String dateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        messageBox.getDatabase().getStatement().executeUpdate("INSERT INTO Message VALUES(null, '"
                + dateTime + "', \"" + messageBox.getUsername() + "\", \""
                + topic.replace("\"", "'").replace("\\", "\\\\")
                + "\", \"" + message.replace("\"", "'").replace("\\", "\\\\")
                + "\", \"" + System.getProperty("user.name") + "\", \"" + recipient + "\", false, false)");
    }

    public ResultSet getDbUsers() throws SQLException {
        return messageBox.getDatabase().getStatement()
                .executeQuery("SELECT User FROM mysql.user where (User not like '%root%') and (User not like '%admin%') " +
                        "and (User not like '%Test%') and (User not like '%test%') and (User not like '') group by User");
    }

    public ResultSet getNewMessages() throws SQLException {
        return messageBox.getDatabase().getStatement()
                .executeQuery("SELECT IdMessage FROM Message WHERE Recipient='" +
                        messageBox.getUsername() + "' AND Readed='0'");
    }

    public void setMessageAsReaded(long id) throws SQLException {
        messageBox.getDatabase().getStatement().executeUpdate("UPDATE Message SET Readed = '1' WHERE IdMessage = '" + id + "'");
    }

    public void setMessageAsDeleted(long id) throws SQLException {
        messageBox.getDatabase().getStatement().executeUpdate("UPDATE Message SET Deleted = '1' WHERE IdMessage = '" + id + "'");
    }

    public void setMessageAsNotDeleted(long id) throws SQLException {
        messageBox.getDatabase().getStatement().executeUpdate("UPDATE Message SET Deleted = '0' WHERE IdMessage = '" + id + "'");
    }

    public ResultSet getDeletedMessages() throws SQLException {
        return messageBox.getDatabase().getStatement()
                .executeQuery("SELECT * FROM Message WHERE Recipient='" +
                        messageBox.getUsername() + "' AND Deleted='1'");
    }

    public ResultSet findMessage(String boxType, String searchingIn, String searchingText, boolean includeDeleted) throws SQLException {
        String query = "SELECT * FROM Message WHERE ";
        if (searchingIn.equals(messageBox.getLanguageFileProperty("topic3")) || searchingIn.contains(messageBox.getLanguageFileProperty("and"))) {
            query += "(Topic LIKE '%" + searchingText + "%'";
        } else if (searchingIn.equals(messageBox.getLanguageFileProperty("content"))) {
            query += "(Message LIKE '%" + searchingText + "%'";
        }
        if (searchingIn.contains(messageBox.getLanguageFileProperty("and"))) {
            query += " OR Message LIKE '%" + searchingText + "%'";
        }
        query += ") AND ";
        if (boxType.equals(messageBox.getLanguageFileProperty("inbox"))) {
            query += "(Recipient='" + messageBox.getUsername();
        } else if (boxType.equals(messageBox.getLanguageFileProperty("outbox"))) {
            query += "(Sender='" + messageBox.getUsername();
        } else {
            query += "(Recipient='" + messageBox.getUsername() + "' OR Sender='" + messageBox.getUsername();
        }
        query += "')";
        if (!includeDeleted) {
            query += " AND Deleted='0'";
        }
        return messageBox.getDatabase().getStatement().executeQuery(query);
    }
}
